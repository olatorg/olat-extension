# OLAT Extension

[![License](https://img.shields.io/badge/License-Apache--2.0-blue)](https://www.apache.org/licenses/LICENSE-2.0)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)
[![OpenOlat](https://img.shields.io/badge/OpenOlat-18.2--LMSUZH--SNAPSHOT-1aa6be)](https://gitlab.com/olatorg/OpenOLAT/-/tree/LMSUZH-OpenOLAT_18.2)
[![Spring Boot](https://img.shields.io/badge/Spring_Boot-3.2.3-6bb536)](https://docs.spring.io/spring-boot/docs/3.2.3/reference/html/)

Collection of UZH specific OpenOLAT extensions.

## Usage

1. Add this repository to the `repositories` section of your project's `pom.xml`.

   ```xml
   <repositories>
     <repository>
       <id>olatorg-repo</id>
       <url>https://gitlab.com/api/v4/groups/olatorg/-/packages/maven</url>
     </repository>
   </repositories>
   ```

2. Add this dependency to your project's `pom.xml`.

   ```xml
   <dependency>
     <groupId>org.olat</groupId>
     <artifactId>olat-extension</artifactId>
     <version>${olat-extension.version}</version>
   </dependency>
   ```

## Docker

OLAT Extension can be used with [Docker](https://www.docker.com/get-started/)
and [Docker Compose](https://docs.docker.com/compose/).

Use the following commands to start OLAT with the provided `docker-compose.yml` file.

1. Authenticate with
   the [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/).

   ```shell
   docker login registry.gitlab.com -u <username> -p <token>
   ```

2. Create a `.env` file to pass environment variables.

   ```shell
   cat > .env<< EOF
   TAG=<tag>
   EOF
   ```

3. Start Docker containers.

   ```shell
   docker-compose up --detach
   ```

4. Stop and remove containers and volumes

   ```shell
   docker-compose down --volumes
   ``` 

## Development

1. Clone the source code.

   ```shell
   git clone git@gitlab.com:olatorg/olat-extension.git
   cd olat-extension
   ```

2. Build project.

   ```shell
   ./mvnw verify
   ```

## License

This project is Open Source software released under
the [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0).
