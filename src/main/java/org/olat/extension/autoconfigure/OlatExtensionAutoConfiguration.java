/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.autoconfigure;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@AutoConfiguration
@EnableConfigurationProperties(OlatExtensionProperties.class)
@ConditionalOnProperty(prefix = "olat.extension", name = "enabled", matchIfMissing = true)
@ComponentScan({
  "org.olat.extension.data.dao",
  "org.olat.extension.injector",
  "org.olat.extension.gta",
  "org.olat.extension.usersearch"
})
public class OlatExtensionAutoConfiguration {}
