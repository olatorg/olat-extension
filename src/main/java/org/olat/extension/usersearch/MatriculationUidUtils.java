/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.usersearch;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Martin Schraner
 * @since 1.2
 */
public class MatriculationUidUtils {

  private MatriculationUidUtils() {}

  private static final Pattern validMatriculationUidPattern;

  static {
    String validMatriculationUidRegex = "\\d+|\\d{2}-\\d{3}-\\d{3}";
    validMatriculationUidPattern = Pattern.compile(validMatriculationUidRegex);
  }

  public static boolean isSearchStringForMatriculationUid(String searchString) {
    if (searchString == null || searchString.isBlank()) {
      return false;
    }
    Matcher matcher = validMatriculationUidPattern.matcher(searchString);
    return matcher.matches();
  }

  public static Optional<Integer> convertMatriculationUidStringToInt(String string) {
    try {
      return Optional.of(Integer.parseInt(string.replace("-", "")));
    } catch (NumberFormatException e) {
      // Not parsable to int
      return Optional.empty();
    }
  }

  /**
   * Convert matriculation uids of type 12345678 to formatted uids of type 12-345-678.
   *
   * @param matriculationUidAsInt Matriculation uid as int
   * @return Formatted matriculation uid
   */
  public static String convertIntToMatriculationUidWithDashes(int matriculationUidAsInt) {
    if (matriculationUidAsInt < 1000000000) {
      int block1 = matriculationUidAsInt / 1000000;
      int remainder1 = matriculationUidAsInt % 1000000;
      int block2 = remainder1 / 1000;
      int block3 = remainder1 % 1000;
      return String.format("%02d-%03d-%03d", block1, block2, block3);
    } else {
      // Uid of foreign student -> do not convert
      return String.valueOf(matriculationUidAsInt);
    }
  }
}
