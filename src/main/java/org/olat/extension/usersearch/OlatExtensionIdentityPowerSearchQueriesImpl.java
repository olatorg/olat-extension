/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.usersearch;

import jakarta.persistence.TypedQuery;
import org.olat.basesecurity.manager.IdentityPowerSearchQueriesImpl;
import org.olat.core.commons.persistence.QueryBuilder;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * Perform extended search of institutional user identifiers (matriculation uids). Unfortunately,
 * matriculation uids are stored in different formats in OLAT. Possible formats are:
 *
 * <ul>
 *   <li>00-123-456 (as string with two dashes)
 *   <li>00123456 (as number with leading zeros)
 *   <li>123456 (as number without leading zeros)
 * </ul>
 *
 * The search has to work for all the possible formats.
 *
 * @author Martin Schraner
 * @since 1.2
 */
@Primary
@Service
public class OlatExtensionIdentityPowerSearchQueriesImpl extends IdentityPowerSearchQueriesImpl {

  private static final String INSTITUTIONAL_USER_IDENTIFIER_AS_INT =
      "institutionalUserIdentifierAsInt";
  private static final String INSTITUTIONAL_USER_IDENTIFIER_WITH_DASHES =
      "institutionalUserIdentifierWithDashes";

  @Override
  protected void appendQueryForInstitutionalUserIdentifier(
      QueryBuilder sb, String key, String value) {
    if (MatriculationUidUtils.isSearchStringForMatriculationUid(value)) {
      sb.append("(TRIM(LEADING '0' FROM user.")
          .append(key)
          .append(") = :")
          .append(INSTITUTIONAL_USER_IDENTIFIER_AS_INT)
          .append(" OR user.")
          .append(key)
          .append(" = :")
          .append(INSTITUTIONAL_USER_IDENTIFIER_WITH_DASHES)
          .append(") ");
    } else {
      super.appendQueryForInstitutionalUserIdentifier(sb, key, value);
    }
  }

  @Override
  protected void fillParametersForInstitutionalUserIdentifier(
      TypedQuery<?> dbq, String key, String value) {
    if (MatriculationUidUtils.isSearchStringForMatriculationUid(value)) {
      int matriculationUidAsInt =
          MatriculationUidUtils.convertMatriculationUidStringToInt(value).orElse(0);
      String matriculationUidWithDashes =
          MatriculationUidUtils.convertIntToMatriculationUidWithDashes(matriculationUidAsInt);
      dbq.setParameter(
          INSTITUTIONAL_USER_IDENTIFIER_AS_INT, Integer.toString(matriculationUidAsInt));
      dbq.setParameter(INSTITUTIONAL_USER_IDENTIFIER_WITH_DASHES, matriculationUidWithDashes);
    } else {
      super.fillParametersForInstitutionalUserIdentifier(dbq, key, value);
    }
  }
}
