/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.usersearch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.olat.basesecurity.ExtendedInstitutionalUserIdentifierSearchService;
import org.olat.basesecurity.IdentityImpl;
import org.olat.basesecurity.model.FindNamedIdentity;
import org.olat.core.id.Identity;
import org.olat.core.id.Organisation;
import org.olat.extension.data.dao.OlatExtensionIdentityDao;
import org.olat.extension.model.IdentityAndInstitutionalUserIdentifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * Perform extended search of institutional user identifiers (matriculation uids) and append search
 * result to list of find named identities. Unfortunately, matriculation uids are stored in
 * different formats in OLAT. Possible formats are:
 *
 * <ul>
 *   <li>00-123-456 (as string with two dashes)
 *   <li>00123456 (as number with leading zeros)
 *   <li>123456 (as number without leading zeros)
 * </ul>
 *
 * The search has to work for all the possible formats.
 *
 * @author Martin Schraner
 * @since 1.1
 */
@Primary
@Service
public class OlatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl
    implements ExtendedInstitutionalUserIdentifierSearchService {

  private final OlatExtensionIdentityDao olatExtensionIdentityDao;

  public OlatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl(
      OlatExtensionIdentityDao olatExtensionIdentityDao) {
    this.olatExtensionIdentityDao = olatExtensionIdentityDao;
  }

  @Override
  public void performAdditionalSearchForFindUserKeyWithPropertyAndAppendResultToUserKeys(
      String searchString, List<Long> userKeys) {

    if (!MatriculationUidUtils.isSearchStringForMatriculationUid(searchString)) {
      return;
    }

    int matriculationUidAsInt =
        MatriculationUidUtils.convertMatriculationUidStringToInt(searchString).orElse(0);
    String matriculationUidWithDashes =
        MatriculationUidUtils.convertIntToMatriculationUidWithDashes(matriculationUidAsInt);

    List<Long> additionalUserKeysFound =
        olatExtensionIdentityDao.findUserIdsByInstitutionalUserIdentifierAsIntOrWithDashes(
            matriculationUidAsInt, matriculationUidWithDashes);

    for (long additionalUserKeyFound : additionalUserKeysFound) {
      if (!userKeys.contains(additionalUserKeyFound)) {
        userKeys.add(additionalUserKeyFound);
      }
    }
  }

  @Override
  public void performAdditionalSearchForFindIdentitiesWithPropertyAndAppendResultToIdentities(
      String searchString, List<Identity> identities) {

    if (!MatriculationUidUtils.isSearchStringForMatriculationUid(searchString)) {
      return;
    }

    int matriculationUidAsInt =
        MatriculationUidUtils.convertMatriculationUidStringToInt(searchString).orElse(0);
    String matriculationUidWithDashes =
        MatriculationUidUtils.convertIntToMatriculationUidWithDashes(matriculationUidAsInt);

    List<IdentityImpl> additionalIdentitiesFound =
        olatExtensionIdentityDao
            .findIdentitiesByInstitutionalUserIdentifierAsIntOrWithDashesWithFetchJoinOfUser(
                matriculationUidAsInt, matriculationUidWithDashes);

    List<Long> identityKeys = identities.stream().map(Identity::getKey).toList();
    for (IdentityImpl additionalIdentityFound : additionalIdentitiesFound) {
      if (!identityKeys.contains(additionalIdentityFound.getKey())) {
        identities.add(additionalIdentityFound);
      }
    }
  }

  @Override
  public void performAdditionalSearchForFindByNamesAndAppendResultToFindNamedIdentities(
      Collection<String> searchStrings,
      List<Organisation> organisations,
      List<FindNamedIdentity> findNamedIdentities) {

    List<String> matriculationUidSearchStrings =
        searchStrings.stream()
            .map(String::trim)
            .distinct()
            .filter(MatriculationUidUtils::isSearchStringForMatriculationUid)
            .toList();
    if (matriculationUidSearchStrings.isEmpty()) {
      return;
    }

    List<IdentityAndInstitutionalUserIdentifier>
        additionalIdentitiesAndInstitutionalUserIdentifiersFound =
            findIdentitiesAndInstitutionalUserIdentifiersByMatriculationUidsWithOrWithoutDashesAndOrganisations(
                matriculationUidSearchStrings, organisations);

    appendAdditionalIdentitiesAndInstitutionalUserIdentifiersFoundToFindNamedIdentities(
        additionalIdentitiesAndInstitutionalUserIdentifiersFound,
        matriculationUidSearchStrings,
        findNamedIdentities);
  }

  List<IdentityAndInstitutionalUserIdentifier>
      findIdentitiesAndInstitutionalUserIdentifiersByMatriculationUidsWithOrWithoutDashesAndOrganisations(
          List<String> matriculationUidSearchStrings, List<Organisation> organisations) {

    List<Long> organisationIds =
        (organisations == null || organisations.isEmpty())
            ? new ArrayList<>()
            : organisations.stream().map(Organisation::getKey).toList();

    List<IdentityAndInstitutionalUserIdentifier> identitiesAndInstitutionalUserIdentifiers =
        new ArrayList<>();

    for (String matriculationUidSearchString : matriculationUidSearchStrings) {
      int matriculationUidAsInt =
          MatriculationUidUtils.convertMatriculationUidStringToInt(matriculationUidSearchString)
              .orElse(0);
      String matriculationUidWithDashes =
          MatriculationUidUtils.convertIntToMatriculationUidWithDashes(matriculationUidAsInt);

      List<IdentityAndInstitutionalUserIdentifier>
          identitiesAndInstitutionalUserIdentifiersOfCurrentSearchString =
              (organisationIds.isEmpty())
                  ? olatExtensionIdentityDao
                      .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashes(
                          matriculationUidAsInt, matriculationUidWithDashes)
                  : olatExtensionIdentityDao
                      .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashesAndOrganisationIdIn(
                          matriculationUidAsInt, matriculationUidWithDashes, organisationIds);

      identitiesAndInstitutionalUserIdentifiers.addAll(
          identitiesAndInstitutionalUserIdentifiersOfCurrentSearchString);
    }

    return identitiesAndInstitutionalUserIdentifiers;
  }

  @SuppressWarnings("java:S3776")
  static void appendAdditionalIdentitiesAndInstitutionalUserIdentifiersFoundToFindNamedIdentities(
      List<IdentityAndInstitutionalUserIdentifier>
          additionalIdentitiesAndInstitutionalUserIdentifiersFound,
      List<String> matriculationUidSearchStrings,
      List<FindNamedIdentity> findNamedIdentities) {

    Map<Long, FindNamedIdentity> mapOfIdentityIdsAndFindNamedIdentities = new HashMap<>();
    for (FindNamedIdentity findNamedIdentity : findNamedIdentities) {
      if (!mapOfIdentityIdsAndFindNamedIdentities.containsKey(
          findNamedIdentity.getIdentity().getKey())) {
        mapOfIdentityIdsAndFindNamedIdentities.put(
            findNamedIdentity.getIdentity().getKey(), findNamedIdentity);
      }
    }

    Map<Integer, String> mapOfMatriculationUidAsIntsAndMatriculationUidSearchStrings =
        new HashMap<>();
    for (String matriculationUidSearchString : matriculationUidSearchStrings) {
      Optional<Integer> matriculationUidAsIntOptional =
          MatriculationUidUtils.convertMatriculationUidStringToInt(matriculationUidSearchString);
      if (matriculationUidAsIntOptional.isPresent()
          && !mapOfMatriculationUidAsIntsAndMatriculationUidSearchStrings.containsKey(
              matriculationUidAsIntOptional.get())) {
        mapOfMatriculationUidAsIntsAndMatriculationUidSearchStrings.put(
            matriculationUidAsIntOptional.get(), matriculationUidSearchString);
      }
    }

    List<FindNamedIdentity> newlyCreatedFindNamedIdentities = new ArrayList<>();
    for (IdentityAndInstitutionalUserIdentifier
        additionalIdentityAndInstitutionalUserIdentifierFound :
            additionalIdentitiesAndInstitutionalUserIdentifiersFound) {

      Identity additionalIdentityFound =
          additionalIdentityAndInstitutionalUserIdentifierFound.identity();
      String additionalInstitutionalUserIdentifierFound =
          additionalIdentityAndInstitutionalUserIdentifierFound.institutionalUserIdentifier();
      Optional<Integer> additionalInstitutionalUserIdentifierAsIntFoundOptional =
          MatriculationUidUtils.convertMatriculationUidStringToInt(
              additionalInstitutionalUserIdentifierFound);

      String matriculationUidSearchString =
          additionalInstitutionalUserIdentifierAsIntFoundOptional
              .map(mapOfMatriculationUidAsIntsAndMatriculationUidSearchStrings::get)
              .orElse(null);
      if (matriculationUidSearchString == null) {
        continue;
      }

      FindNamedIdentity findNamedIdentityToBeUpdated =
          mapOfIdentityIdsAndFindNamedIdentities.get(additionalIdentityFound.getKey());
      if (findNamedIdentityToBeUpdated != null) {
        if (!findNamedIdentityToBeUpdated
            .getNamesLowerCase()
            .contains(matriculationUidSearchString)) {
          findNamedIdentityToBeUpdated.addName(matriculationUidSearchString);
        }
      } else {
        FindNamedIdentity newlyCreatedFindNamedIdentity =
            new FindNamedIdentity(additionalIdentityFound);
        newlyCreatedFindNamedIdentity.addName(matriculationUidSearchString);
        newlyCreatedFindNamedIdentities.add(newlyCreatedFindNamedIdentity);
      }
    }

    findNamedIdentities.addAll(newlyCreatedFindNamedIdentities);
  }
}
