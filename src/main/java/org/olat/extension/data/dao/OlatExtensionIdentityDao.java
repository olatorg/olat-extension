/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.data.dao;

import java.util.List;
import org.olat.basesecurity.IdentityImpl;
import org.olat.core.commons.persistence.DB;
import org.olat.extension.model.IdentityAndInstitutionalUserIdentifier;
import org.springframework.stereotype.Component;

/**
 * @author Martin Schraner
 * @since 1.1
 */
@Component
public class OlatExtensionIdentityDao {

  private static final String INSTITUTIONAL_USER_IDENTIFIER_AS_INT =
      "institutionalUserIdentifierAsInt";
  private static final String INSTITUTIONAL_USER_IDENTIFIER_WITH_DASHES =
      "institutionalUserIdentifierWithDashes";
  private static final String ORGANISATION_IDS = "organisationIds";

  private final DB dbInstance;

  public OlatExtensionIdentityDao(DB dbInstance) {
    this.dbInstance = dbInstance;
  }

  public List<Long> findUserIdsByInstitutionalUserIdentifierAsIntOrWithDashes(
      int institutionalUserIdentifierAsInt, String institutionalUserIdentifierWithDashes) {
    return dbInstance
        .getCurrentEntityManager()
        .createQuery(
            "SELECT u.key FROM UserImpl u "
                + "WHERE TRIM(LEADING '0' FROM u.institutionalUserIdentifier) "
                + "   = :institutionalUserIdentifierAsInt "
                + "OR u.institutionalUserIdentifier = :institutionalUserIdentifierWithDashes",
            Long.class)
        .setParameter(
            INSTITUTIONAL_USER_IDENTIFIER_AS_INT,
            Integer.toString(institutionalUserIdentifierAsInt))
        .setParameter(
            INSTITUTIONAL_USER_IDENTIFIER_WITH_DASHES, institutionalUserIdentifierWithDashes)
        .getResultList();
  }

  public List<IdentityImpl>
      findIdentitiesByInstitutionalUserIdentifierAsIntOrWithDashesWithFetchJoinOfUser(
          int institutionalUserIdentifierAsInt, String institutionalUserIdentifierWithDashes) {
    return dbInstance
        .getCurrentEntityManager()
        .createQuery(
            "SELECT i FROM IdentityImpl i INNER JOIN FETCH i.user u "
                + "WHERE TRIM(LEADING '0' FROM u.institutionalUserIdentifier) "
                + "   = :institutionalUserIdentifierAsInt "
                + "OR u.institutionalUserIdentifier = :institutionalUserIdentifierWithDashes",
            IdentityImpl.class)
        .setParameter(
            INSTITUTIONAL_USER_IDENTIFIER_AS_INT,
            Integer.toString(institutionalUserIdentifierAsInt))
        .setParameter(
            INSTITUTIONAL_USER_IDENTIFIER_WITH_DASHES, institutionalUserIdentifierWithDashes)
        .getResultList();
  }

  @SuppressWarnings("JpaQlInspection")
  public List<IdentityAndInstitutionalUserIdentifier>
      findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashes(
          int institutionalUserIdentifierAsInt, String institutionalUserIdentifierWithDashes) {
    return dbInstance
        .getCurrentEntityManager()
        .createQuery(
            "SELECT new org.olat.extension.model."
                + "IdentityAndInstitutionalUserIdentifier(u.identity, u.institutionalUserIdentifier) "
                + "FROM UserImpl u "
                + "WHERE TRIM(LEADING '0' FROM u.institutionalUserIdentifier) "
                + "   = :institutionalUserIdentifierAsInt "
                + "OR u.institutionalUserIdentifier = :institutionalUserIdentifierWithDashes",
            IdentityAndInstitutionalUserIdentifier.class)
        .setParameter(
            INSTITUTIONAL_USER_IDENTIFIER_AS_INT,
            Integer.toString(institutionalUserIdentifierAsInt))
        .setParameter(
            INSTITUTIONAL_USER_IDENTIFIER_WITH_DASHES, institutionalUserIdentifierWithDashes)
        .getResultList();
  }

  @SuppressWarnings("JpaQlInspection")
  public List<IdentityAndInstitutionalUserIdentifier>
      findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashesAndOrganisationIdIn(
          int institutionalUserIdentifierAsInt,
          String institutionalUserIdentifierWithDashes,
          List<Long> organisationIds) {
    return dbInstance
        .getCurrentEntityManager()
        .createQuery(
            "SELECT new org.olat.extension.model."
                + "IdentityAndInstitutionalUserIdentifier(u.identity, u.institutionalUserIdentifier) "
                + "FROM UserImpl u "
                + "WHERE (TRIM(LEADING '0' FROM u.institutionalUserIdentifier) "
                + "      = :institutionalUserIdentifierAsInt "
                + "   OR u.institutionalUserIdentifier = :institutionalUserIdentifierWithDashes) "
                + "AND EXISTS ("
                + "   SELECT gm.key FROM bgroupmember gm "
                + "   INNER JOIN organisation o ON (o.group.key = gm.group.key) "
                + "   WHERE gm.identity.key = u.identity.key AND o.key IN (:organisationIds))",
            IdentityAndInstitutionalUserIdentifier.class)
        .setParameter(
            INSTITUTIONAL_USER_IDENTIFIER_AS_INT,
            Integer.toString(institutionalUserIdentifierAsInt))
        .setParameter(
            INSTITUTIONAL_USER_IDENTIFIER_WITH_DASHES, institutionalUserIdentifierWithDashes)
        .setParameter(ORGANISATION_IDS, organisationIds)
        .getResultList();
  }
}
