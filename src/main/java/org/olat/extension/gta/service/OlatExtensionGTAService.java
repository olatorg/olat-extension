/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.gta.service;

import java.util.List;
import java.util.Map;
import org.olat.core.gui.translator.Translator;
import org.olat.core.id.Identity;
import org.olat.core.util.mail.MailContext;
import org.olat.course.nodes.GTACourseNode;
import org.olat.group.BusinessGroup;
import org.olat.repository.RepositoryEntry;

/**
 * @author Martin Schraner
 * @since 1.0
 */
public interface OlatExtensionGTAService {

  void addUniqueIdentities(Map<Long, Identity> map, List<Identity> list);

  List<Identity> getCourseOwners(RepositoryEntry repositoryEntry);

  List<Identity> getCourseCoaches(RepositoryEntry repositoryEntry);

  List<Identity> getGroupCoaches(GTACourseNode gtaNode, RepositoryEntry courseEntry);

  List<Identity> addRecipients(
      RepositoryEntry courseEntry, GTACourseNode gtaNode, Identity assessedIdentity);

  List<Identity> addRecipientsForGroupTask(
      RepositoryEntry courseEntry, GTACourseNode gtaNode, BusinessGroup assessedGroup);

  void sendGradedEmail(
      GTACourseNode gtaNode,
      Identity assessedIdentity,
      List<Identity> recipients,
      String subject,
      String taskName,
      MailContext context,
      Translator translator);

  void sendGradedEmailForGroupTask(
      GTACourseNode gtaNode,
      BusinessGroup assessedGroup,
      List<Identity> recipients,
      String subject,
      String taskName,
      MailContext context,
      Translator translator);
}
