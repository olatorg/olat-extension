/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.gta.service.impl;

import java.util.*;
import org.olat.basesecurity.BaseSecurity;
import org.olat.basesecurity.GroupRoles;
import org.olat.basesecurity.SearchIdentityParams;
import org.olat.core.gui.translator.Translator;
import org.olat.core.id.Identity;
import org.olat.core.util.StringHelper;
import org.olat.core.util.mail.MailContext;
import org.olat.core.util.mail.MailManager;
import org.olat.core.util.mail.MailTemplate;
import org.olat.course.condition.Condition;
import org.olat.course.nodes.GTACourseNode;
import org.olat.extension.gta.mailtemplate.GTAAssessmentMailTemplate;
import org.olat.extension.gta.mailtemplate.GTAAssessmentMailTemplateForGroupTask;
import org.olat.extension.gta.service.OlatExtensionGTAService;
import org.olat.group.BusinessGroup;
import org.olat.group.BusinessGroupMembership;
import org.olat.group.BusinessGroupService;
import org.olat.group.model.SearchBusinessGroupParams;
import org.olat.modules.ModuleConfiguration;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.RepositoryEntryRelationType;
import org.olat.repository.manager.RepositoryEntryRelationDAO;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Service
public class OlatExtensionGTAServiceImpl implements OlatExtensionGTAService {

  private final BusinessGroupService businessGroupService;
  private final MailManager mailManager;
  private final BaseSecurity baseSecurity;
  private final RepositoryEntryRelationDAO repositoryEntryRelationDao;

  /* Note: Due to several cyclic dependencies of the OpenOLAT beans, it's important that the
   * olat-lms beans don't affect the order in which the OpenOLAT beans are loaded. This can be
   * achieved by Spring's @Lazy annotation, which should always be used when autowiring OpenOLAT
   * services from olat-lms beans.
   */
  public OlatExtensionGTAServiceImpl(
      @Lazy BusinessGroupService businessGroupService,
      @Lazy MailManager mailManager,
      @Lazy BaseSecurity baseSecurity,
      @Lazy RepositoryEntryRelationDAO repositoryEntryRelationDao) {
    this.businessGroupService = businessGroupService;
    this.baseSecurity = baseSecurity;
    this.mailManager = mailManager;
    this.repositoryEntryRelationDao = repositoryEntryRelationDao;
  }

  @Override
  public void addUniqueIdentities(Map<Long, Identity> map, List<Identity> list) {
    list.forEach(
        identity -> {
          if (!map.containsKey(identity.getKey())) {
            map.put(identity.getKey(), identity);
          }
        });
  }

  @Override
  public List<Identity> getCourseOwners(RepositoryEntry repositoryEntry) {
    return repositoryEntryRelationDao.getMembers(
        repositoryEntry, RepositoryEntryRelationType.defaultGroup, GroupRoles.owner.name());
  }

  @Override
  public List<Identity> getCourseCoaches(RepositoryEntry repositoryEntry) {
    return repositoryEntryRelationDao.getMembers(
        repositoryEntry, RepositoryEntryRelationType.defaultGroup, GroupRoles.coach.name());
  }

  @Override
  public List<Identity> getGroupCoaches(GTACourseNode gtaNode, RepositoryEntry courseEntry) {

    Condition visibilityCondition = gtaNode.getPreConditionVisibility();
    List<Long> identityKeysOfGroupCoaches = new ArrayList<>();

    if (visibilityCondition != null) {
      // Get groups from visibility settings of course node
      SearchBusinessGroupParams groupSearchParams =
          new SearchBusinessGroupParams(null, false, false);
      groupSearchParams.setGroupKeys(visibilityCondition.getEasyModeGroupAccessIdList());
      List<BusinessGroup> groups =
          businessGroupService.findBusinessGroups(groupSearchParams, courseEntry, 0, -1);

      // Get group memberships and related identity keys for group coaches
      List<BusinessGroupMembership> memberships =
          businessGroupService.getBusinessGroupsMembership(groups);
      for (BusinessGroupMembership membership : memberships) {
        if (membership.isOwner()) {
          identityKeysOfGroupCoaches.add(membership.getIdentityKey());
        }
      }
    }

    SearchIdentityParams identitySearchParams = new SearchIdentityParams();
    identitySearchParams.setIdentityKeys(identityKeysOfGroupCoaches);
    return (!identityKeysOfGroupCoaches.isEmpty())
        ? baseSecurity.getIdentitiesByPowerSearch(identitySearchParams, 0, -1)
        : new ArrayList<>();
  }

  @Override
  public List<Identity> addRecipients(
      RepositoryEntry courseEntry, GTACourseNode gtaNode, Identity assessedIdentity) {

    ModuleConfiguration config = gtaNode.getModuleConfiguration();
    Map<Long, Identity> recipients = new HashMap<>();

    if (config.getBooleanSafe(GTACourseNode.GTASK_ASSESSMENT_MAIL_CONFIRMATION_OWNER)) {
      addUniqueIdentities(recipients, getCourseOwners(courseEntry));
    }
    if (config.getBooleanSafe(GTACourseNode.GTASK_ASSESSMENT_MAIL_CONFIRMATION_COACH_COURSE)) {
      addUniqueIdentities(recipients, getCourseCoaches(courseEntry));
    }
    if (config.getBooleanSafe(GTACourseNode.GTASK_ASSESSMENT_MAIL_CONFIRMATION_COACH_GROUP)) {
      addUniqueIdentities(recipients, getGroupCoaches(gtaNode, courseEntry));
    }
    if (config.getBooleanSafe(GTACourseNode.GTASK_ASSESSMENT_MAIL_CONFIRMATION_PARTICIPANT)) {
      addUniqueIdentities(recipients, Collections.singletonList(assessedIdentity));
    }
    return new ArrayList<>(recipients.values());
  }

  @Override
  public List<Identity> addRecipientsForGroupTask(
      RepositoryEntry courseEntry, GTACourseNode gtaNode, BusinessGroup assessedGroup) {

    ModuleConfiguration config = gtaNode.getModuleConfiguration();
    Map<Long, Identity> recipients = new HashMap<>();

    if (config.getBooleanSafe(GTACourseNode.GTASK_ASSESSMENT_MAIL_CONFIRMATION_OWNER)) {
      addUniqueIdentities(recipients, getCourseOwners(courseEntry));
    }
    if (config.getBooleanSafe(GTACourseNode.GTASK_ASSESSMENT_MAIL_CONFIRMATION_COACH_COURSE)) {
      addUniqueIdentities(
          recipients, businessGroupService.getMembers(assessedGroup, GroupRoles.coach.name()));
    }
    if (config.getBooleanSafe(GTACourseNode.GTASK_ASSESSMENT_MAIL_CONFIRMATION_COACH_GROUP)) {
      addUniqueIdentities(recipients, getGroupCoaches(gtaNode, courseEntry));
    }
    if (config.getBooleanSafe(GTACourseNode.GTASK_ASSESSMENT_MAIL_CONFIRMATION_PARTICIPANT)) {
      addUniqueIdentities(
          recipients,
          businessGroupService.getMembers(assessedGroup, GroupRoles.participant.name()));
    }
    return new ArrayList<>(recipients.values());
  }

  @Override
  public void sendGradedEmail(
      GTACourseNode gtaNode,
      Identity assessedIdentity,
      List<Identity> recipients,
      String subject,
      String taskName,
      MailContext context,
      Translator translator) {

    ModuleConfiguration config = gtaNode.getModuleConfiguration();
    String body = config.getStringValue(GTACourseNode.GTASK_ASSESSMENT_TEXT);

    if (StringHelper.containsNonWhitespace(body)) {
      MailTemplate mailTemplate =
          new GTAAssessmentMailTemplate(subject, body, taskName, assessedIdentity, translator);
      mailManager.sendToRecipientsList(context, mailTemplate, recipients);
    }
  }

  @Override
  public void sendGradedEmailForGroupTask(
      GTACourseNode gtaNode,
      BusinessGroup assessedGroup,
      List<Identity> recipients,
      String subject,
      String taskName,
      MailContext context,
      Translator translator) {

    ModuleConfiguration config = gtaNode.getModuleConfiguration();
    String body = config.getStringValue(GTACourseNode.GTASK_ASSESSMENT_TEXT);

    if (StringHelper.containsNonWhitespace(body)) {
      MailTemplate mailTemplate =
          new GTAAssessmentMailTemplateForGroupTask(
              subject, body, taskName, assessedGroup, translator);
      if (!recipients.isEmpty()) {
        mailManager.sendToRecipientsList(context, mailTemplate, recipients);
      }
    }
  }
}
