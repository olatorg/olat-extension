/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.gta.ui;

import static org.olat.extension.gta.ui.OlatExtensionAbstractAssignmentEditControllerUtil.*;

import org.olat.core.commons.services.notifications.SubscriptionContext;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItem;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.FormLink;
import org.olat.core.gui.components.form.flexible.elements.MultipleSelectionElement;
import org.olat.core.gui.components.form.flexible.elements.RichTextElement;
import org.olat.core.gui.components.form.flexible.impl.FormEvent;
import org.olat.core.gui.components.form.flexible.impl.FormLayoutContainer;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.util.StringHelper;
import org.olat.core.util.Util;
import org.olat.course.nodes.GTACourseNode;
import org.olat.course.nodes.gta.ui.GTAAssignmentEditController;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.modules.ModuleConfiguration;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SuppressWarnings("java:S110")
public class OlatExtensionGTAAssignmentEditController extends GTAAssignmentEditController {

  private static final String[] emailRecipientKeys = GTACourseNode.emailRecipientKeys;

  private final SubscriptionContext subscriptionContext;

  private BulkUploadTasksController addMultipleTasksCtrl;
  private FormLink addMultipleTasksLink;
  private FormLayoutContainer emailConfirmationContainer;
  private RichTextElement emailTextEl;
  private MultipleSelectionElement emailRecipientEl;

  public OlatExtensionGTAAssignmentEditController(
      UserRequest ureq,
      WindowControl wControl,
      GTACourseNode gtaNode,
      ModuleConfiguration config,
      CourseEnvironment courseEnv,
      boolean readOnly) {
    super(
        ureq,
        wControl,
        gtaNode,
        config,
        courseEnv,
        readOnly,
        Util.createPackageTranslator(GTAAssignmentEditController.class, ureq.getLocale()));
    subscriptionContext = createSubscriptionContext(gtaManager, gtaNode, courseEnv);

    // Must be called after initialization of instance variables!
    super.initForm(ureq);
  }

  @Override
  protected void initForm(UserRequest ureq) {
    // NOOP
  }

  @Override
  protected void addAdditionalElementsToTaskContainer(FormLayoutContainer tasksCont) {
    addMultipleTasksLink = addAddMultipleTasksLink(tasksCont, uifactory);
  }

  @Override
  protected boolean displaySpacerElement() {
    return false;
  }

  @Override
  protected void addEmailConfirmationElements(FormItemContainer formLayout) {
    addEmailConfirmationContainer(formLayout);
    addEmailTextElement();
    addEmailRecipientElement();
  }

  private void addEmailConfirmationContainer(FormItemContainer formLayout) {
    emailConfirmationContainer =
        FormLayoutContainer.createDefaultFormLayout("confirmation", getTranslator());
    emailConfirmationContainer.setFormTitle(translate("assignment.confirmation.title"));
    emailConfirmationContainer.setRootForm(mainForm);
    formLayout.add(emailConfirmationContainer);
  }

  private void addEmailTextElement() {
    String mailText = config.getStringValue(GTACourseNode.GTASK_ASSIGNMENT_TEXT);
    if (!StringHelper.containsNonWhitespace(mailText)) {
      mailText = translate("assignment.email.template");
    }
    emailTextEl =
        uifactory.addRichTextElementForStringDataMinimalistic(
            "task.mail.text",
            "assignment.email.label",
            mailText,
            10,
            -1,
            emailConfirmationContainer,
            getWindowControl());
    emailTextEl.setMandatory(true);
  }

  private void addEmailRecipientElement() {
    String[] emailRecipientValues =
        new String[] {
          translate("email.recipient.owner"),
          translate("email.recipient.coach.course"),
          translate("email.recipient.coach.group"),
          translate("email.recipient.participant")
        };
    emailRecipientEl =
        uifactory.addCheckboxesHorizontal(
            "email.recipient.roles",
            "assignment.email.confirmation.roles",
            emailConfirmationContainer,
            emailRecipientKeys,
            emailRecipientValues);
    emailRecipientEl.select(
        emailRecipientKeys[0],
        config.getBooleanSafe(GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_OWNER));
    emailRecipientEl.select(
        emailRecipientKeys[1],
        config.getBooleanSafe(GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_COACH_COURSE));
    emailRecipientEl.select(
        emailRecipientKeys[2],
        config.getBooleanSafe(GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_COACH_GROUP));
    emailRecipientEl.select(
        emailRecipientKeys[3],
        config.getBooleanSafe(GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_PARTICIPANT));
  }

  @Override
  protected void addSaveAndCancelButton(UserRequest ureq, FormLayoutContainer configCont) {
    FormLayoutContainer buttonsCont =
        FormLayoutContainer.createButtonLayout("buttons", getTranslator());
    buttonsCont.setElementCssClass("o_sel_course_gta_task_config_buttons");
    buttonsCont.setRootForm(mainForm);
    emailConfirmationContainer.add(buttonsCont);
    uifactory.addFormSubmitButton("save", buttonsCont);
    uifactory.addFormCancelButton("cancel", buttonsCont, ureq, getWindowControl());
  }

  @Override
  protected void processEmailConfirmationElements() {
    processEmailTextElement();
    processEmailRecipientElement();
  }

  private void processEmailTextElement() {
    String emailText = emailTextEl.getValue();
    config.setStringValue(GTACourseNode.GTASK_ASSIGNMENT_TEXT, emailText);
  }

  private void processEmailRecipientElement() {
    boolean isOwnerSelected = emailRecipientEl.isSelected(0);
    config.setBooleanEntry(GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_OWNER, isOwnerSelected);
    boolean isCoachCourseSelected = emailRecipientEl.isSelected(1);
    config.setBooleanEntry(
        GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_COACH_COURSE, isCoachCourseSelected);
    boolean isCoachGroupSelected = emailRecipientEl.isSelected(2);
    config.setBooleanEntry(
        GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_COACH_GROUP, isCoachGroupSelected);
    boolean isParticipantSelected = emailRecipientEl.isSelected(3);
    config.setBooleanEntry(
        GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_PARTICIPANT, isParticipantSelected);
  }

  @SuppressWarnings("DuplicatedCode")
  @Override
  protected void event(UserRequest ureq, Controller source, Event event) {
    if (addMultipleTasksCtrl == source) {
      if (event == Event.DONE_EVENT) {
        addTaskDefinitions(addMultipleTasksCtrl, gtaManager, courseEnv, gtaNode);
        fireEvent(ureq, Event.DONE_EVENT);
        updateModel(ureq);
        updateNotificationsManager(notificationsManager, subscriptionContext);
      }
      cmc.deactivate();
      cleanUp();
    }
    super.event(ureq, source, event);
  }

  @Override
  protected void cleanUp() {
    removeAsListenerAndDispose(addMultipleTasksCtrl);
    addMultipleTasksCtrl = null;
    super.cleanUp();
  }

  @Override
  protected void formInnerEvent(UserRequest ureq, FormItem source, FormEvent event) {
    if (addMultipleTasksLink == source) {
      String cmcTitle = translate("add.multipleTasks");
      AddMultipleTasksCtrlAndCmc updatedAddMultipleTasksCtrlAndCmc =
          doAddMultipleTasks(cmcTitle, ureq, tasksFolder, this);
      addMultipleTasksCtrl = updatedAddMultipleTasksCtrlAndCmc.addMultipleTasksCtrl();
      cmc = updatedAddMultipleTasksCtrlAndCmc.cmc();
    }
    super.formInnerEvent(ureq, source, event);
  }
}
