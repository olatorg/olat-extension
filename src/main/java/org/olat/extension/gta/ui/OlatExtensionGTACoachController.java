/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.gta.ui;

import java.util.List;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.id.Identity;
import org.olat.core.id.Roles;
import org.olat.core.util.Util;
import org.olat.core.util.mail.MailContext;
import org.olat.core.util.mail.MailContextImpl;
import org.olat.course.nodes.GTACourseNode;
import org.olat.course.nodes.gta.GTAType;
import org.olat.course.nodes.gta.Task;
import org.olat.course.nodes.gta.ui.GTACoachController;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.course.run.userview.UserCourseEnvironment;
import org.olat.extension.gta.service.OlatExtensionGTAService;
import org.olat.group.BusinessGroup;
import org.olat.repository.RepositoryEntry;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SuppressWarnings("java:S6813")
public class OlatExtensionGTACoachController extends GTACoachController {

  @Autowired private OlatExtensionGTAService olatExtensionGTAService;

  @SuppressWarnings("java:S107")
  public OlatExtensionGTACoachController(
      UserRequest ureq,
      WindowControl wControl,
      CourseEnvironment courseEnv,
      GTACourseNode gtaNode,
      UserCourseEnvironment coachCourseEnv,
      BusinessGroup assessedGroup,
      boolean withTitle,
      boolean withGrading,
      boolean withSubscription,
      boolean withReset) {
    super(
        ureq,
        wControl,
        courseEnv,
        gtaNode,
        coachCourseEnv,
        assessedGroup,
        withTitle,
        withGrading,
        withSubscription,
        withReset,
        Util.createPackageTranslator(GTACoachController.class, ureq.getLocale()));
  }

  @SuppressWarnings("java:S107")
  public OlatExtensionGTACoachController(
      UserRequest ureq,
      WindowControl wControl,
      CourseEnvironment courseEnv,
      GTACourseNode gtaNode,
      UserCourseEnvironment coachCourseEnv,
      Identity assessedIdentity,
      boolean withTitle,
      boolean withGrading,
      boolean withSubscription,
      boolean withReset) {
    super(
        ureq,
        wControl,
        courseEnv,
        gtaNode,
        coachCourseEnv,
        assessedIdentity,
        withTitle,
        withGrading,
        withSubscription,
        withReset,
        Util.createPackageTranslator(GTACoachController.class, ureq.getLocale()));
  }

  @Override
  protected boolean isDisplayChangeLogConfig(UserRequest ureq) {
    Roles roles = ureq.getUserSession().getRoles();
    if (roles.isAdministrator() || roles.isAuthor()) {
      return true;
    }
    return super.isDisplayChangeLogConfig(ureq);
  }

  @Override
  protected void doGradedEmail(
      RepositoryEntry courseEntry, GTACourseNode gtaNode, Task assignedTask) {

    String subject = translate("assessment.email.subject");
    MailContext mailContext =
        new MailContextImpl(getWindowControl().getBusinessControl().getAsString());

    if (GTAType.group.name().equals(config.getStringValue(GTACourseNode.GTASK_TYPE))) {
      List<Identity> recipients =
          olatExtensionGTAService.addRecipientsForGroupTask(courseEntry, gtaNode, assessedGroup);
      olatExtensionGTAService.sendGradedEmailForGroupTask(
          gtaNode,
          assessedGroup,
          recipients,
          subject,
          assignedTask.getTaskName(),
          mailContext,
          getTranslator());
    } else {
      List<Identity> recipients =
          olatExtensionGTAService.addRecipients(courseEntry, gtaNode, assessedIdentity);
      olatExtensionGTAService.sendGradedEmail(
          gtaNode,
          assessedIdentity,
          recipients,
          subject,
          assignedTask.getTaskName(),
          mailContext,
          getTranslator());
    }
  }
}
