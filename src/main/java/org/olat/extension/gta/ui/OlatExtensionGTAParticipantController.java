/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.gta.ui;

import java.io.File;
import java.util.*;
import org.olat.basesecurity.GroupRoles;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.translator.PackageTranslator;
import org.olat.core.gui.translator.Translator;
import org.olat.core.id.Identity;
import org.olat.core.id.Roles;
import org.olat.core.id.UserConstants;
import org.olat.core.util.Formatter;
import org.olat.core.util.StringHelper;
import org.olat.core.util.Util;
import org.olat.core.util.mail.*;
import org.olat.course.nodes.GTACourseNode;
import org.olat.course.nodes.gta.GTAType;
import org.olat.course.nodes.gta.Task;
import org.olat.course.nodes.gta.TaskHelper;
import org.olat.course.nodes.gta.model.DueDate;
import org.olat.course.nodes.gta.ui.GTAParticipantController;
import org.olat.course.run.userview.UserCourseEnvironment;
import org.olat.extension.gta.mailtemplate.GTAAssignmentMailTemplate;
import org.olat.extension.gta.mailtemplate.GTASubmissionMailTemplate;
import org.olat.extension.gta.service.OlatExtensionGTAService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SuppressWarnings("java:S6813")
public class OlatExtensionGTAParticipantController extends GTAParticipantController {

  private static final String ASSIGNMENT_EMAIL_TEMPLATE_KEY = "assignment.email.template";

  @Autowired private MailManager mailManager;
  @Autowired private OlatExtensionGTAService olatExtensionGTAService;

  public OlatExtensionGTAParticipantController(
      UserRequest ureq,
      WindowControl wControl,
      GTACourseNode gtaNode,
      UserCourseEnvironment userCourseEnv) {
    super(
        ureq,
        wControl,
        gtaNode,
        userCourseEnv,
        Util.createPackageTranslator(GTAParticipantController.class, ureq.getLocale()));
  }

  @Override
  protected boolean isDisplayChangeLogConfig(UserRequest ureq) {
    Roles roles = ureq.getUserSession().getRoles();
    if (roles.isAdministrator() || roles.isAuthor()) {
      return true;
    }
    return super.isDisplayChangeLogConfig(ureq);
  }

  @SuppressWarnings("DuplicatedCode")
  @Override
  protected void showTaskSuccessfullyAssignedInfo(Task assignedTask, Identity identity) {
    String message = config.getStringValue(GTACourseNode.GTASK_ASSIGNMENT_TEXT);
    if (!StringHelper.containsNonWhitespace(message)) {
      message = translate(ASSIGNMENT_EMAIL_TEMPLATE_KEY);
    }
    DueDate dueDate = getSubmissionDueDate(assignedTask);
    Date due = (dueDate == null) ? null : dueDate.getDueDate();
    Translator translator = getTranslator();
    Locale locale = translator.getLocale();

    // Prepare a HashMap with placeholders for the template
    HashMap<String, String> placeholders = new HashMap<>();
    placeholders.put("login", identity.getName());
    placeholders.put("first", identity.getUser().getProperty(UserConstants.FIRSTNAME, locale));
    placeholders.put("firstName", identity.getUser().getProperty(UserConstants.FIRSTNAME, locale));
    placeholders.put("last", identity.getUser().getProperty(UserConstants.LASTNAME, locale));
    placeholders.put("lastName", identity.getUser().getProperty(UserConstants.LASTNAME, locale));
    placeholders.put("email", identity.getUser().getProperty(UserConstants.EMAIL, locale));
    placeholders.put("title", assignedTask.getTaskName());

    // Format all dates using Formatter
    Date now = new Date();
    org.olat.core.util.Formatter f = Formatter.getInstance(locale);
    placeholders.put("date", f.formatDate(now));
    placeholders.put("time", f.formatTime(now));
    placeholders.put("dueDate", due == null ? "" : f.formatDate(due));
    placeholders.put("dueTime", due == null ? "" : f.formatTime(due));

    // Replace placeholders
    for (Map.Entry<String, String> entry : placeholders.entrySet()) {
      String placeholderKey = entry.getKey();
      String placeholderValue = entry.getValue();
      message = message.replaceAll("\\$" + placeholderKey, placeholderValue);
    }

    getWindowControl().setInfo(message);
  }

  @Override
  protected void doAssignmentEmail(Task assignedTask) {
    // Get email body from course configuration
    String emailBody = config.getStringValue(GTACourseNode.GTASK_ASSIGNMENT_TEXT);
    if (!StringHelper.containsNonWhitespace(emailBody)) {
      return;
    }

    // Replace email text by correct language / don't show due date information if due date is
    // not set (only possible if standard text is used for email)
    DueDate dueDate = getSubmissionDueDate(assignedTask);
    if (isStandardTextUsedForEmail(emailBody, ASSIGNMENT_EMAIL_TEMPLATE_KEY)) {
      emailBody =
          (dueDate == null)
              ? "<p>" + translate("assignment.email.template.no.due.date") + "</p>"
              : "<p>" + translate(ASSIGNMENT_EMAIL_TEMPLATE_KEY) + "</p>";
    }

    // Build the list of recipients for the message according to configuration of the course element
    boolean sendToOwners =
        config.getBooleanSafe(GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_OWNER);
    boolean sendToCourseCoaches =
        config.getBooleanSafe(GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_COACH_COURSE);
    boolean sendToGroupCoaches =
        config.getBooleanSafe(GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_COACH_GROUP);
    boolean sendToParticipants =
        config.getBooleanSafe(GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_PARTICIPANT);
    List<Identity> recipients =
        addRecipients(sendToOwners, sendToParticipants, sendToCourseCoaches, sendToGroupCoaches);

    // Prepare mail template
    String subject = translate("assignment.email.subject");
    Date submissionDueDate = (dueDate == null) ? null : dueDate.getDueDate();
    MailTemplate template =
        new GTAAssignmentMailTemplate(
            subject,
            emailBody,
            submissionDueDate,
            assignedTask.getTaskName(),
            getIdentity(),
            getTranslator());

    // Send message to all found recipients
    if (!recipients.isEmpty()) {
      sendMail(template, recipients);
    }
  }

  @Override
  protected boolean isSubmissionEmailRequired() {
    return true;
  }

  @Override
  protected void doSubmissionEmail(Task assignedTask) {
    // Get email body from course configuration
    String emailBody = config.getStringValue(GTACourseNode.GTASK_SUBMISSION_TEXT);
    if (!StringHelper.containsNonWhitespace(emailBody)) {
      return;
    }

    // Replace email text by correct language (only possible if standard text is used for email)
    if (isStandardTextUsedForEmail(emailBody, "submission.email.template")) {
      emailBody = "<p>" + translate("submission.email.template") + "</p>";
    }

    // Build the list of recipients for the message according to configuration of the course element
    boolean sendToOwners =
        config.getBooleanSafe(GTACourseNode.GTASK_SUBMISSION_MAIL_CONFIRMATION_OWNER);
    boolean sendToCourseCoaches =
        config.getBooleanSafe(GTACourseNode.GTASK_SUBMISSION_MAIL_CONFIRMATION_COACH_COURSE);
    boolean sendToGroupCoaches =
        config.getBooleanSafe(GTACourseNode.GTASK_SUBMISSION_MAIL_CONFIRMATION_COACH_GROUP);
    // Backwards compatibility to allow old setting that did not consider different roles
    boolean sendToParticipants =
        config.getBooleanSafe(GTACourseNode.GTASK_SUBMISSION_MAIL_CONFIRMATION_PARTICIPANT)
            || config.getBooleanSafe(GTACourseNode.GTASK_SUBMISSION_MAIL_CONFIRMATION);
    List<Identity> recipients =
        addRecipients(sendToOwners, sendToParticipants, sendToCourseCoaches, sendToGroupCoaches);

    // Prepare mail template
    String subject = translate("submission.email.subject");
    DueDate dueDate = getSubmissionDueDate(assignedTask);
    Date submissionDueDate = (dueDate == null) ? null : dueDate.getDueDate();
    File submitDirectory =
        (GTAType.group.name().equals(config.getStringValue(GTACourseNode.GTASK_TYPE)))
            ? gtaManager.getSubmitDirectory(courseEnv, gtaNode, assessedGroup)
            : gtaManager.getSubmitDirectory(courseEnv, gtaNode, assessedIdentity);
    File[] files = TaskHelper.getDocuments(submitDirectory);
    MailTemplate template =
        new GTASubmissionMailTemplate(
            subject, emailBody, submissionDueDate, files, getIdentity(), getTranslator());

    // Send message to all found recipients
    if (!recipients.isEmpty()) {
      sendMail(template, recipients);
    }
  }

  private boolean isStandardTextUsedForEmail(String emailBody, String standardTextKey) {
    String emailText = emailBody.replace("<p>", "");
    emailText = emailText.replace("</p>", "");

    // Check for all supported languages if email text equals to assignment email template
    Locale[] supportedLocales = {Locale.GERMAN, Locale.ENGLISH, Locale.FRENCH, Locale.ITALIAN};
    Translator translatorToBeUsedForAssignmentText =
        new PackageTranslator(getTranslator().getPackageName(), null);
    for (Locale locale : supportedLocales) {
      translatorToBeUsedForAssignmentText.setLocale(locale);
      String assignmentEmailTemplate =
          translatorToBeUsedForAssignmentText.translate(standardTextKey);
      if (emailText.equals(assignmentEmailTemplate)) {
        return true;
      }
    }
    return false;
  }

  private List<Identity> addRecipients(
      boolean addOwners,
      boolean addParticipants,
      boolean addCourseCoaches,
      boolean addGroupCoaches) {

    Map<Long, Identity> recipients = new HashMap<>();
    if (addOwners) {
      olatExtensionGTAService.addUniqueIdentities(
          recipients, olatExtensionGTAService.getCourseOwners(courseEntry));
    }
    if (addCourseCoaches) {
      olatExtensionGTAService.addUniqueIdentities(
          recipients, olatExtensionGTAService.getCourseCoaches(courseEntry));
    }
    if (addGroupCoaches) {
      if (GTAType.group.name().equals(config.getStringValue(GTACourseNode.GTASK_TYPE))) {
        olatExtensionGTAService.addUniqueIdentities(
            recipients, businessGroupService.getMembers(assessedGroup, GroupRoles.coach.name()));
      } else {
        olatExtensionGTAService.addUniqueIdentities(
            recipients, olatExtensionGTAService.getGroupCoaches(gtaNode, courseEntry));
      }
    }
    if (addParticipants) {
      if (GTAType.group.name().equals(config.getStringValue(GTACourseNode.GTASK_TYPE))) {
        olatExtensionGTAService.addUniqueIdentities(
            recipients,
            businessGroupService.getMembers(assessedGroup, GroupRoles.participant.name()));
      } else {
        olatExtensionGTAService.addUniqueIdentities(
            recipients, Collections.singletonList(assessedIdentity));
      }
    }
    return new ArrayList<>(recipients.values());
  }

  private void sendMail(MailTemplate template, List<Identity> recipients) {
    MailContext context =
        new MailContextImpl(
            null,
            null,
            getWindowControl().getBusinessControl().getAsString(),
            this.courseEntry.getDisplayname());
    MailerResult result = new MailerResult();
    MailBundle[] bundles =
        mailManager.makeMailBundles(
            context, recipients, template, null, UUID.randomUUID().toString(), result);
    mailManager.sendMessage(bundles);
  }
}
