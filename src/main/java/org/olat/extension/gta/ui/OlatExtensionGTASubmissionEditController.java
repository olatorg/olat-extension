/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.gta.ui;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.MultipleSelectionElement;
import org.olat.core.gui.components.form.flexible.elements.RichTextElement;
import org.olat.core.gui.components.form.flexible.impl.FormLayoutContainer;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.util.StringHelper;
import org.olat.core.util.Util;
import org.olat.core.util.mail.MailHelper;
import org.olat.course.nodes.GTACourseNode;
import org.olat.course.nodes.gta.ui.GTAMailTemplate;
import org.olat.course.nodes.gta.ui.GTASubmissionEditController;
import org.olat.modules.ModuleConfiguration;

/**
 * @author Martin Schraner
 * @since 1.0
 */
public class OlatExtensionGTASubmissionEditController extends GTASubmissionEditController {

  private static final String[] emailRecipientKeys =
      new String[] {
        GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_OWNER,
        GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_COACH_COURSE,
        GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_COACH_GROUP,
        GTACourseNode.GTASK_ASSIGNMENT_MAIL_CONFIRMATION_PARTICIPANT
      };

  private RichTextElement emailTextEl;
  private MultipleSelectionElement emailRecipientEl;

  public OlatExtensionGTASubmissionEditController(
      UserRequest ureq, WindowControl wControl, ModuleConfiguration config) {
    super(
        ureq,
        wControl,
        config,
        Util.createPackageTranslator(GTASubmissionEditController.class, ureq.getLocale()));
  }

  @Override
  protected FormLayoutContainer addEmailConfirmationElements(
      String[] enableValues, FormItemContainer formLayout) {
    FormLayoutContainer confirmationContainer = addConfirmationContainer(formLayout);
    addEmailTextElement(confirmationContainer);
    addEmailRecipientElement(confirmationContainer);
    return confirmationContainer;
  }

  private FormLayoutContainer addConfirmationContainer(FormItemContainer formLayout) {
    FormLayoutContainer confirmationCont =
        FormLayoutContainer.createDefaultFormLayout("confirmation", getTranslator());
    confirmationCont.setFormTitle(translate("submission.confirmation.title"));
    confirmationCont.setRootForm(mainForm);
    formLayout.add(confirmationCont);
    return confirmationCont;
  }

  private void addEmailTextElement(FormLayoutContainer confirmationCont) {
    String text = config.getStringValue(GTACourseNode.GTASK_SUBMISSION_TEXT);
    if (!StringHelper.containsNonWhitespace(text)) {
      text = translate("submission.email.template");
    }
    emailTextEl =
        uifactory.addRichTextElementForStringDataMinimalistic(
            "text", "submission.text", text, 10, -1, confirmationCont, getWindowControl());
    emailTextEl.setMandatory(true);
    MailHelper.setVariableNamesAsHelp(emailTextEl, GTAMailTemplate.variableNames(), getLocale());
  }

  private void addEmailRecipientElement(FormLayoutContainer confirmationCont) {
    String[] emailRecipientValues =
        new String[] {
          translate("email.recipient.owner"),
          translate("email.recipient.coach.course"),
          translate("email.recipient.coach.group"),
          translate("email.recipient.participant")
        };
    emailRecipientEl =
        uifactory.addCheckboxesHorizontal(
            "email.recipient.roles",
            "submission.email.confirmation.roles",
            confirmationCont,
            emailRecipientKeys,
            emailRecipientValues);
    emailRecipientEl.select(
        emailRecipientKeys[0],
        config.getBooleanSafe(GTACourseNode.GTASK_SUBMISSION_MAIL_CONFIRMATION_OWNER));
    emailRecipientEl.select(
        emailRecipientKeys[1],
        config.getBooleanSafe(GTACourseNode.GTASK_SUBMISSION_MAIL_CONFIRMATION_COACH_COURSE));
    emailRecipientEl.select(
        emailRecipientKeys[2],
        config.getBooleanSafe(GTACourseNode.GTASK_SUBMISSION_MAIL_CONFIRMATION_COACH_GROUP));

    // When getting config setting for participant, also consider existing value for the "old"
    // schema that did not consider roles
    emailRecipientEl.select(
        emailRecipientKeys[3],
        config.getBooleanSafe(GTACourseNode.GTASK_SUBMISSION_MAIL_CONFIRMATION_PARTICIPANT)
            || config.getBooleanSafe(GTACourseNode.GTASK_SUBMISSION_MAIL_CONFIRMATION));
  }

  @Override
  protected void updateEmailConfirmationElements() {
    // NOOP
  }

  @Override
  protected void processEmailConfirmationElements() {
    processEmailTextElement();
    processEmailRecipientElement();
  }

  private void processEmailTextElement() {
    String text = emailTextEl.getValue();
    config.setStringValue(GTACourseNode.GTASK_SUBMISSION_TEXT, text);
  }

  private void processEmailRecipientElement() {
    boolean isOwnerSelected = emailRecipientEl.isSelected(0);
    config.setBooleanEntry(GTACourseNode.GTASK_SUBMISSION_MAIL_CONFIRMATION_OWNER, isOwnerSelected);
    boolean isCoachCourseSelected = emailRecipientEl.isSelected(1);
    config.setBooleanEntry(
        GTACourseNode.GTASK_SUBMISSION_MAIL_CONFIRMATION_COACH_COURSE, isCoachCourseSelected);
    boolean isCoachGroupSelected = emailRecipientEl.isSelected(2);
    config.setBooleanEntry(
        GTACourseNode.GTASK_SUBMISSION_MAIL_CONFIRMATION_COACH_GROUP, isCoachGroupSelected);
    boolean isParticipantSelected = emailRecipientEl.isSelected(3);
    config.setBooleanEntry(
        GTACourseNode.GTASK_SUBMISSION_MAIL_CONFIRMATION_PARTICIPANT, isParticipantSelected);

    // Explicitly remove old setting to not interfere with role based approach
    config.remove(GTACourseNode.GTASK_SUBMISSION_MAIL_CONFIRMATION);
  }
}
