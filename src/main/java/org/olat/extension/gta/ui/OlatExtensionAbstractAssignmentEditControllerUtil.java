/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.gta.ui;

import java.io.File;
import java.util.List;
import org.olat.core.commons.services.notifications.NotificationsManager;
import org.olat.core.commons.services.notifications.SubscriptionContext;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormUIFactory;
import org.olat.core.gui.components.form.flexible.elements.FormLink;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.FormLayoutContainer;
import org.olat.core.gui.components.link.Link;
import org.olat.core.gui.control.generic.closablewrapper.CloseableModalController;
import org.olat.course.nodes.GTACourseNode;
import org.olat.course.nodes.gta.GTAManager;
import org.olat.course.nodes.gta.model.TaskDefinition;
import org.olat.course.run.environment.CourseEnvironment;

/**
 * @author Martin Schraner
 * @since 1.0
 */
class OlatExtensionAbstractAssignmentEditControllerUtil {

  record AddMultipleTasksCtrlAndCmc(
      BulkUploadTasksController addMultipleTasksCtrl, CloseableModalController cmc) {}

  static SubscriptionContext createSubscriptionContext(
      GTAManager gtaManager, GTACourseNode gtaNode, CourseEnvironment courseEnv) {
    return gtaManager.getSubscriptionContext(
        courseEnv.getCourseGroupManager().getCourseResource(), gtaNode, false);
  }

  static FormLink addAddMultipleTasksLink(FormLayoutContainer tasksCont, FormUIFactory uiFactory) {
    FormLink addMultipleTasksLink =
        uiFactory.addFormLink("add.multipleTasks", tasksCont, Link.BUTTON);
    addMultipleTasksLink.setElementCssClass("o_sel_course_gta_add_multipleTasks");
    addMultipleTasksLink.setIconLeftCSS("o_icon o_icon_upload");
    return addMultipleTasksLink;
  }

  static void addTaskDefinitions(
      BulkUploadTasksController addMultipleTasksCtrl,
      GTAManager gtaManager,
      CourseEnvironment courseEnv,
      GTACourseNode gtaNode) {
    List<TaskDefinition> newTaskList = addMultipleTasksCtrl.getTaskList();
    for (TaskDefinition newTask : newTaskList) {
      gtaManager.addTaskDefinition(newTask, courseEnv, gtaNode);
    }
  }

  static void updateNotificationsManager(
      NotificationsManager notificationsManager, SubscriptionContext subscriptionContext) {
    notificationsManager.markPublisherNews(subscriptionContext, null, false);
  }

  static AddMultipleTasksCtrlAndCmc doAddMultipleTasks(
      String cmcTitle,
      UserRequest ureq,
      File tasksFolder,
      FormBasicController formBasicController) {

    BulkUploadTasksController addMultipleTasksCtrl =
        new BulkUploadTasksController(ureq, formBasicController.getWindowControl(), tasksFolder);
    formBasicController.listenTo(addMultipleTasksCtrl);

    CloseableModalController cmc =
        new CloseableModalController(
            formBasicController.getWindowControl(),
            null,
            addMultipleTasksCtrl.getInitialComponent(),
            true,
            cmcTitle,
            false);
    formBasicController.listenTo(cmc);
    cmc.activate();

    return new AddMultipleTasksCtrlAndCmc(addMultipleTasksCtrl, cmc);
  }
}
