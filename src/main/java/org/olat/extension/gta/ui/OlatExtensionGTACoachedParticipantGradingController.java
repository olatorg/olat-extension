/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.gta.ui;

import java.util.List;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.id.Identity;
import org.olat.core.id.OLATResourceable;
import org.olat.core.util.Util;
import org.olat.core.util.mail.MailContext;
import org.olat.core.util.mail.MailContextImpl;
import org.olat.course.nodes.GTACourseNode;
import org.olat.course.nodes.gta.Task;
import org.olat.course.nodes.gta.ui.GTACoachedParticipantGradingController;
import org.olat.course.run.userview.UserCourseEnvironment;
import org.olat.extension.gta.service.OlatExtensionGTAService;
import org.olat.repository.RepositoryEntry;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SuppressWarnings("java:S6813")
public class OlatExtensionGTACoachedParticipantGradingController
    extends GTACoachedParticipantGradingController {

  @Autowired private OlatExtensionGTAService olatExtensionGTAService;

  public OlatExtensionGTACoachedParticipantGradingController(
      UserRequest ureq,
      WindowControl wControl,
      OLATResourceable courseOres,
      GTACourseNode gtaNode,
      Task assignedTask,
      UserCourseEnvironment coachCourseEnv,
      Identity assessedIdentity) {
    super(
        ureq,
        wControl,
        courseOres,
        gtaNode,
        assignedTask,
        coachCourseEnv,
        assessedIdentity,
        Util.createPackageTranslator(
            GTACoachedParticipantGradingController.class, ureq.getLocale()));
  }

  @Override
  protected void doGradedEmail(
      UserCourseEnvironment coachCourseEnv,
      GTACourseNode gtaNode,
      Task assignedTask,
      Identity assessedIdentity) {

    RepositoryEntry courseEntry =
        coachCourseEnv.getCourseEnvironment().getCourseGroupManager().getCourseEntry();

    // Build the list of recipients for the message according to configuration of the course element
    List<Identity> recipients =
        olatExtensionGTAService.addRecipients(courseEntry, gtaNode, assessedIdentity);

    String subject = translate("assessment.email.subject");
    String repositoryEntryDisplayName = courseEntry.getDisplayname();
    String businessPath = getWindowControl().getBusinessControl().getAsString();
    MailContext context = new MailContextImpl(null, null, businessPath, repositoryEntryDisplayName);
    olatExtensionGTAService.sendGradedEmail(
        gtaNode,
        assessedIdentity,
        recipients,
        subject,
        assignedTask.getTaskName(),
        context,
        getTranslator());
  }
}
