/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.gta.ui;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.control.WindowControl;
import org.olat.course.nodes.GTACourseNode;
import org.olat.course.nodes.gta.ui.GTACoachAssignementEditController;
import org.olat.course.nodes.gta.ui.GTACoachAssignmentEditControllerFactory;
import org.olat.course.run.environment.CourseEnvironment;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@Primary
@Component
public class OlatExtensionGTACoachAssignmentEditControllerFactoryImpl
    implements GTACoachAssignmentEditControllerFactory {

  @Override
  public GTACoachAssignementEditController create(
      UserRequest ureq,
      WindowControl wControl,
      GTACourseNode gtaNode,
      CourseEnvironment courseEnv,
      boolean readOnly) {
    return new OlatExtensionGTACoachAssignmentEditController(
        ureq, wControl, gtaNode, courseEnv, readOnly);
  }
}
