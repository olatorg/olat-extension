/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.gta.ui;

import static org.olat.extension.gta.ui.OlatExtensionAbstractAssignmentEditControllerUtil.*;

import org.olat.core.commons.services.notifications.SubscriptionContext;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItem;
import org.olat.core.gui.components.form.flexible.elements.FormLink;
import org.olat.core.gui.components.form.flexible.impl.FormEvent;
import org.olat.core.gui.components.form.flexible.impl.FormLayoutContainer;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.util.Util;
import org.olat.course.nodes.GTACourseNode;
import org.olat.course.nodes.gta.ui.GTACoachAssignementEditController;
import org.olat.course.run.environment.CourseEnvironment;

/**
 * @author Martin Schraner
 * @since 1.0
 */
@SuppressWarnings("java:S110")
public class OlatExtensionGTACoachAssignmentEditController
    extends GTACoachAssignementEditController {

  private final SubscriptionContext subscriptionContext;

  private BulkUploadTasksController addMultipleTasksCtrl;
  private FormLink addMultipleTasksLink;

  public OlatExtensionGTACoachAssignmentEditController(
      UserRequest ureq,
      WindowControl wControl,
      GTACourseNode gtaNode,
      CourseEnvironment courseEnv,
      boolean readOnly) {
    super(
        ureq,
        wControl,
        gtaNode,
        courseEnv,
        readOnly,
        Util.createPackageTranslator(GTACoachAssignementEditController.class, ureq.getLocale()));
    subscriptionContext = createSubscriptionContext(gtaManager, gtaNode, courseEnv);

    // Must be called after initialization of instance variables!
    super.initForm(ureq);
  }

  @Override
  protected void initForm(UserRequest ureq) {
    // NOOP
  }

  @Override
  protected void addAdditionalElementsToTaskContainer(FormLayoutContainer tasksCont) {
    addMultipleTasksLink = addAddMultipleTasksLink(tasksCont, uifactory);
  }

  @SuppressWarnings("DuplicatedCode")
  @Override
  protected void event(UserRequest ureq, Controller source, Event event) {
    if (addMultipleTasksCtrl == source) {
      if (event == Event.DONE_EVENT) {
        addTaskDefinitions(addMultipleTasksCtrl, gtaManager, courseEnv, gtaNode);
        fireEvent(ureq, Event.DONE_EVENT);
        updateModel(ureq);
        updateNotificationsManager(notificationsManager, subscriptionContext);
      }
      cmc.deactivate();
      cleanUp();
    }
    super.event(ureq, source, event);
  }

  @Override
  protected void cleanUp() {
    removeAsListenerAndDispose(addMultipleTasksCtrl);
    addMultipleTasksCtrl = null;
    super.cleanUp();
  }

  @Override
  protected void formInnerEvent(UserRequest ureq, FormItem source, FormEvent event) {
    if (addMultipleTasksLink == source) {
      String cmcTitle = translate("add.multipleTasks");
      AddMultipleTasksCtrlAndCmc updatedAddMultipleTasksCtrlAndCmc =
          doAddMultipleTasks(cmcTitle, ureq, tasksFolder, this);
      addMultipleTasksCtrl = updatedAddMultipleTasksCtrlAndCmc.addMultipleTasksCtrl();
      cmc = updatedAddMultipleTasksCtrlAndCmc.cmc();
    }
    super.formInnerEvent(ureq, source, event);
  }
}
