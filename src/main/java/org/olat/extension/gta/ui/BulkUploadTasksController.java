/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.gta.ui;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import lombok.Getter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.FileElement;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.FormEvent;
import org.olat.core.gui.components.form.flexible.impl.FormLayoutContainer;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.util.ZipUtil;
import org.olat.course.nodes.gta.model.TaskDefinition;

/**
 * @author furredir, dirk.furrer@uzh.ch
 * @since 1.0
 */
public class BulkUploadTasksController extends FormBasicController {

  private static final Set<String> zipMimeTypes = new HashSet<>();

  static {
    zipMimeTypes.add("application/zip");
  }

  private FileElement fileEl;

  @Getter List<TaskDefinition> taskList;
  private final File taskContainer;

  public BulkUploadTasksController(UserRequest ureq, WindowControl wControl, File taskContainer) {
    super(ureq, wControl);
    this.taskList = new ArrayList<>();
    this.taskContainer = taskContainer;
    initForm(ureq);
  }

  @Override
  protected void initForm(FormItemContainer formLayout, Controller listener, UserRequest ureq) {
    formLayout.setElementCssClass("o_sel_course_gta_upload_task_form");

    fileEl =
        uifactory.addFileElement(
            getWindowControl(), ureq.getIdentity(), "file", "zip.file", formLayout);
    fileEl.setExampleKey("add.multipleTasks.hint", null);
    fileEl.limitToMimeType(zipMimeTypes, "add.multipleTasks.mimeLimit", null);
    fileEl.setMandatory(true);
    fileEl.addActionListener(FormEvent.ONCHANGE);

    FormLayoutContainer buttonCont =
        FormLayoutContainer.createButtonLayout("buttons", getTranslator());
    buttonCont.setRootForm(mainForm);
    formLayout.add(buttonCont);
    uifactory.addFormSubmitButton("save", buttonCont);
    uifactory.addFormCancelButton("cancel", buttonCont, ureq, getWindowControl());
  }

  @Override
  protected boolean validateFormLogic(UserRequest ureq) {
    boolean allOk = true;

    fileEl.clearError();
    if (fileEl.getInitialFile() == null && fileEl.getUploadFile() == null) {
      fileEl.setErrorKey("form.mandatory.hover");
      allOk = false;
    }

    boolean validationOfParentClassOk = super.validateFormLogic(ureq);
    if (!validationOfParentClassOk) {
      allOk = false;
    }

    return allOk;
  }

  @Override
  protected void formOK(UserRequest ureq) {
    File unzipDir = new File(taskContainer, "unzipDir");
    ZipUtil.unzip(fileEl.getUploadFile(), unzipDir);

    try (Stream<Path> pathStream = Files.walk(Paths.get(unzipDir.getPath()))) {
      List<File> newFiles =
          pathStream
              .filter(Files::isRegularFile)
              .filter(p -> !p.getFileName().toString().startsWith("."))
              .map(Path::toFile)
              .toList();

      for (File taskFile : newFiles) {
        addTaskFileToTaskList(taskFile);
      }

      FileUtils.deleteDirectory(unzipDir);
    } catch (IOException e) {
      logWarn("Cannot delete directory " + unzipDir + ": ", e);
    }
    fireEvent(ureq, Event.DONE_EVENT);
  }

  private void addTaskFileToTaskList(File taskFile) {
    if (taskFile.isFile()) {
      try {
        Path upload = taskFile.toPath();
        File target = new File(taskContainer, taskFile.getName());
        Files.move(upload, target.toPath());
        TaskDefinition task = new TaskDefinition();
        task.setFilename(taskFile.getName());
        task.setTitle(FilenameUtils.getBaseName(taskFile.getName()));
        taskList.add(task);
      } catch (FileAlreadyExistsException e) {
        logDebug(
            "taskFile was not added because it already existed: " + taskFile.getAbsolutePath());
      } catch (Exception e) {
        logError("", e);
      }
    }
  }

  @Override
  protected void formCancelled(UserRequest ureq) {
    fireEvent(ureq, Event.CANCELLED_EVENT);
  }
}
