/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.gta.mailtemplate;

import java.util.Date;
import java.util.Locale;
import org.apache.velocity.VelocityContext;
import org.olat.core.gui.translator.Translator;
import org.olat.core.id.Identity;
import org.olat.core.id.UserConstants;
import org.olat.core.util.Formatter;
import org.olat.core.util.mail.MailTemplate;

public class GTAAssessmentMailTemplate extends MailTemplate {

  private final Identity identity;
  private final Translator translator;
  private final String taskName;

  public GTAAssessmentMailTemplate(
      String subject, String body, String taskName, Identity identity, Translator translator) {
    super(subject, body, null);
    this.translator = translator;
    this.identity = identity;
    this.taskName = taskName;
  }

  @SuppressWarnings("DuplicatedCode")
  @Override
  public void putVariablesInMailContext(VelocityContext context, Identity recipient) {
    Locale locale = translator.getLocale();

    // Compatibility with the old TA
    context.put("login", identity.getName());
    context.put("first", identity.getUser().getProperty(UserConstants.FIRSTNAME, locale));
    context.put("firstName", identity.getUser().getProperty(UserConstants.FIRSTNAME, locale));
    context.put("last", identity.getUser().getProperty(UserConstants.LASTNAME, locale));
    context.put("lastName", identity.getUser().getProperty(UserConstants.LASTNAME, locale));
    context.put("email", identity.getUser().getProperty(UserConstants.EMAIL, locale));
    context.put("title", taskName);

    // Format all dates using Formatter
    Date now = new Date();
    Formatter f = Formatter.getInstance(locale);
    context.put("date", f.formatDate(now));
    context.put("time", f.formatTime(now));
  }
}
