/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.injector;

import ch.uzh.olat.lms.QuotaProgressBarInjector;
import org.olat.core.commons.modules.bc.components.FolderComponent;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.ComponentEventListener;
import org.olat.core.gui.components.progressbar.ProgressBar;
import org.olat.core.gui.components.velocity.VelocityContainer;
import org.olat.core.gui.control.Event;
import org.olat.core.util.vfs.Quota;
import org.olat.core.util.vfs.VFSContainer;
import org.olat.core.util.vfs.VFSManager;
import org.olat.core.util.vfs.callbacks.VFSSecurityCallback;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Christian Schweizer
 * @since 1.0
 */
@Component
@Scope("prototype")
public class QuotaProgressBarInjectorImpl
    implements QuotaProgressBarInjector, ComponentEventListener {

  private final ProgressBar progressBar;
  private FolderComponent folderComponent;

  public QuotaProgressBarInjectorImpl() {
    progressBar = new ProgressBar("quotaProgressBar");
    progressBar.setWidth(200);
    progressBar.setUnitLabel("MB");
    progressBar.setLabelAlignment(ProgressBar.LabelAlignment.right);
  }

  @Override
  public void inject(VelocityContainer container, FolderComponent folderComponent) {
    this.folderComponent = folderComponent;
    folderComponent.addListener(this);
    calculateActualQuota(folderComponent);
    container.put(progressBar.getComponentName(), progressBar);
  }

  @Override
  public void dispatchEvent(
      UserRequest userRequest, org.olat.core.gui.components.Component source, Event event) {
    if (source == folderComponent) {
      calculateActualQuota(folderComponent);
    }
  }

  private void calculateActualQuota(FolderComponent folderComponent) {
    long quotaKB = Quota.UNLIMITED;
    long actualUsage;
    VFSContainer currentContainer = folderComponent.getCurrentContainer();
    VFSContainer inheritingContainer =
        VFSManager.findInheritingSecurityCallbackContainer(currentContainer);
    if (inheritingContainer != null) {
      VFSSecurityCallback securityCallback = inheritingContainer.getLocalSecurityCallback();
      actualUsage = VFSManager.getUsageKB(inheritingContainer);
      progressBar.setActual(actualUsage / 1024f);
      if (inheritingContainer.getLocalSecurityCallback().getQuota() != null) {
        quotaKB = securityCallback.getQuota().getQuotaKB();
      }
    }
    if (quotaKB == Quota.UNLIMITED) {
      progressBar.setIsNoMax(true);
    } else if (quotaKB == 0) {
      progressBar.setMax(quotaKB);
    } else {
      progressBar.setMax(quotaKB / 1024f);
    }
  }
}
