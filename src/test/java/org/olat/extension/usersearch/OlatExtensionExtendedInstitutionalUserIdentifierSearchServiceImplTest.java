/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.usersearch;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.olat.basesecurity.IdentityImpl;
import org.olat.basesecurity.IdentityRef;
import org.olat.basesecurity.model.FindNamedIdentity;
import org.olat.core.id.Identity;
import org.olat.core.id.Organisation;
import org.olat.core.id.UserConstants;
import org.olat.extension.data.dao.OlatExtensionIdentityDao;
import org.olat.extension.model.IdentityAndInstitutionalUserIdentifier;
import org.olat.user.UserImpl;

/**
 * @author Martin Schraner
 * @since 1.1
 */
@ExtendWith(MockitoExtension.class)
class OlatExtensionExtendedInstitutionalUserIdentifierSearchServiceImplTest {

  @Mock private OlatExtensionIdentityDao olatExtensionIdentityDao;

  private IdentityImpl identity1;
  private IdentityImpl identity2;
  private IdentityImpl identity5;
  private UserImpl user1;
  private UserImpl user2;
  private UserImpl user5;
  private IdentityAndInstitutionalUserIdentifier identityAndInstitutionalUserIdentifier1;
  private IdentityAndInstitutionalUserIdentifier identityAndInstitutionalUserIdentifier2;
  private IdentityAndInstitutionalUserIdentifier identityAndInstitutionalUserIdentifier3;
  private IdentityAndInstitutionalUserIdentifier identityAndInstitutionalUserIdentifier5;

  private OlatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl
      olatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl;

  @BeforeEach
  void init() {
    olatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl =
        new OlatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl(
            olatExtensionIdentityDao);
  }

  private void setupTestdata() {
    identity1 = new IdentityImpl();
    identity1.setKey(101L);
    identity1.setName("u10001");

    identity2 = new IdentityImpl();
    identity2.setKey(102L);
    identity2.setName("u10002");

    IdentityImpl identity3 = new IdentityImpl();
    identity3.setKey(103L);
    identity3.setName("u10003");

    IdentityImpl identity4 = new IdentityImpl();
    identity4.setKey(104L);
    identity4.setName("u10004");

    identity5 = new IdentityImpl();
    identity5.setKey(105L);
    identity5.setName("u10005");

    IdentityImpl identity6 = new IdentityImpl();
    identity6.setKey(106L);
    identity6.setName("u10006");

    user1 = new UserImpl();
    user1.setKey(10001L);
    user1.setUserProperty(UserConstants.INSTITUTIONALUSERIDENTIFIER, "11-111-111");

    user2 = new UserImpl();
    user2.setKey(10002L);
    user2.setUserProperty(UserConstants.INSTITUTIONALUSERIDENTIFIER, "22-222-222");

    UserImpl user3 = new UserImpl();
    user3.setKey(10003L);
    user3.setUserProperty(UserConstants.INSTITUTIONALUSERIDENTIFIER, "00055555");

    UserImpl user4 = new UserImpl();
    user4.setKey(10004L);
    user4.setUserProperty(UserConstants.INSTITUTIONALUSERIDENTIFIER, "555555");

    user5 = new UserImpl();
    user5.setKey(10005L);
    user5.setUserProperty(UserConstants.INSTITUTIONALUSERIDENTIFIER, "05-555-555");

    UserImpl user6 = new UserImpl();
    user6.setKey(10006L);
    user6.setUserProperty(UserConstants.INSTITUTIONALUSERIDENTIFIER, "55555555");

    identityAndInstitutionalUserIdentifier1 =
        new IdentityAndInstitutionalUserIdentifier(identity1, "11-111-111");
    identityAndInstitutionalUserIdentifier2 =
        new IdentityAndInstitutionalUserIdentifier(identity2, "22-222-222");
    identityAndInstitutionalUserIdentifier3 =
        new IdentityAndInstitutionalUserIdentifier(identity3, "00055555");
    identityAndInstitutionalUserIdentifier5 =
        new IdentityAndInstitutionalUserIdentifier(identity5, "05-555-555");
  }

  @Test
  void testPerformAdditionalSearchForFindUserKeyWithPropertyAndAppendResultToUserKeys() {

    setupTestdata();

    List<Long> userKeys = new ArrayList<>(List.of(user1.getKey(), user2.getKey()));

    when(olatExtensionIdentityDao.findUserIdsByInstitutionalUserIdentifierAsIntOrWithDashes(
            555555, "00-555-555"))
        .thenReturn(List.of(user5.getKey()));

    olatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl
        .performAdditionalSearchForFindUserKeyWithPropertyAndAppendResultToUserKeys(
            "00555555", userKeys);

    assertEquals(3, userKeys.size());
    assertTrue(userKeys.contains(user1.getKey()));
    assertTrue(userKeys.contains(user2.getKey()));
    assertTrue(userKeys.contains(user5.getKey()));
  }

  @Test
  void testPerformAdditionalSearchForFindIdentitiesWithPropertyAndAppendResultToIdentities() {

    setupTestdata();

    List<Identity> identities = new ArrayList<>(List.of(identity1, identity2));

    when(olatExtensionIdentityDao
            .findIdentitiesByInstitutionalUserIdentifierAsIntOrWithDashesWithFetchJoinOfUser(
                555555, "00-555-555"))
        .thenReturn(List.of(identity5));

    olatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl
        .performAdditionalSearchForFindIdentitiesWithPropertyAndAppendResultToIdentities(
            "00555555", identities);

    assertEquals(3, identities.size());
    List<Long> identityIds = identities.stream().map(IdentityRef::getKey).toList();
    assertTrue(identityIds.contains(identity1.getKey()));
    assertTrue(identityIds.contains(identity2.getKey()));
    assertTrue(identityIds.contains(identity5.getKey()));
  }

  @Test
  void
      testFindIdentitiesAndInstitutionalUserIdentifiersByMatriculationUidsWithOrWithoutDashesAndOrganisations_organisationsEmpty() {

    setupTestdata();

    when(olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashes(
                11111111, "11-111-111"))
        .thenReturn(List.of(identityAndInstitutionalUserIdentifier1));
    when(olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashes(
                22222222, "22-222-222"))
        .thenReturn(List.of(identityAndInstitutionalUserIdentifier2));

    List<IdentityAndInstitutionalUserIdentifier> identityAndInstitutionalUserIdentifiers =
        olatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl
            .findIdentitiesAndInstitutionalUserIdentifiersByMatriculationUidsWithOrWithoutDashesAndOrganisations(
                List.of("11111111", "22-222-222"), new ArrayList<>());
    assertEquals(2, identityAndInstitutionalUserIdentifiers.size());
    List<Long> identityIds =
        identityAndInstitutionalUserIdentifiers.stream()
            .map(
                identityAndInstitutionalUserIdentifier ->
                    identityAndInstitutionalUserIdentifier.identity().getKey())
            .toList();
    assertTrue(identityIds.contains(101L));
    assertTrue(identityIds.contains(102L));
    List<String> institutionalUserIdentifiers =
        identityAndInstitutionalUserIdentifiers.stream()
            .map(IdentityAndInstitutionalUserIdentifier::institutionalUserIdentifier)
            .toList();
    assertTrue(institutionalUserIdentifiers.contains("11-111-111"));
    assertTrue(institutionalUserIdentifiers.contains("22-222-222"));

    identityAndInstitutionalUserIdentifiers =
        olatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl
            .findIdentitiesAndInstitutionalUserIdentifiersByMatriculationUidsWithOrWithoutDashesAndOrganisations(
                List.of("11-111-111", "22222222"), new ArrayList<>());
    assertEquals(2, identityAndInstitutionalUserIdentifiers.size());

    when(olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashes(
                5555555, "05-555-555"))
        .thenReturn(List.of(identityAndInstitutionalUserIdentifier5));

    identityAndInstitutionalUserIdentifiers =
        olatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl
            .findIdentitiesAndInstitutionalUserIdentifiersByMatriculationUidsWithOrWithoutDashesAndOrganisations(
                List.of("05-555-555"), new ArrayList<>());
    assertEquals(1, identityAndInstitutionalUserIdentifiers.size());
    assertEquals(
        identityAndInstitutionalUserIdentifier5, identityAndInstitutionalUserIdentifiers.get(0));

    identityAndInstitutionalUserIdentifiers =
        olatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl
            .findIdentitiesAndInstitutionalUserIdentifiersByMatriculationUidsWithOrWithoutDashesAndOrganisations(
                List.of("05555555"), new ArrayList<>());
    assertEquals(1, identityAndInstitutionalUserIdentifiers.size());
    assertEquals(
        identityAndInstitutionalUserIdentifier5, identityAndInstitutionalUserIdentifiers.get(0));

    identityAndInstitutionalUserIdentifiers =
        olatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl
            .findIdentitiesAndInstitutionalUserIdentifiersByMatriculationUidsWithOrWithoutDashesAndOrganisations(
                List.of("5555555"), new ArrayList<>());
    assertEquals(1, identityAndInstitutionalUserIdentifiers.size());
    assertEquals(
        identityAndInstitutionalUserIdentifier5, identityAndInstitutionalUserIdentifiers.get(0));

    when(olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashes(
                55555, "00-055-555"))
        .thenReturn(List.of(identityAndInstitutionalUserIdentifier3));

    identityAndInstitutionalUserIdentifiers =
        olatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl
            .findIdentitiesAndInstitutionalUserIdentifiersByMatriculationUidsWithOrWithoutDashesAndOrganisations(
                List.of("00-055-555"), new ArrayList<>());
    assertEquals(1, identityAndInstitutionalUserIdentifiers.size());
    assertEquals(
        identityAndInstitutionalUserIdentifier3, identityAndInstitutionalUserIdentifiers.get(0));

    identityAndInstitutionalUserIdentifiers =
        olatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl
            .findIdentitiesAndInstitutionalUserIdentifiersByMatriculationUidsWithOrWithoutDashesAndOrganisations(
                List.of("00055555"), new ArrayList<>());
    assertEquals(1, identityAndInstitutionalUserIdentifiers.size());
    assertEquals(
        identityAndInstitutionalUserIdentifier3, identityAndInstitutionalUserIdentifiers.get(0));

    identityAndInstitutionalUserIdentifiers =
        olatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl
            .findIdentitiesAndInstitutionalUserIdentifiersByMatriculationUidsWithOrWithoutDashesAndOrganisations(
                List.of("55555"), new ArrayList<>());
    assertEquals(1, identityAndInstitutionalUserIdentifiers.size());
    assertEquals(
        identityAndInstitutionalUserIdentifier3, identityAndInstitutionalUserIdentifiers.get(0));
  }

  @Test
  void
      testFindIdentitiesAndInstitutionalUserIdentifiersByMatriculationUidsWithOrWithoutDashesAndOrganisations_organisationsNotEmpty() {

    setupTestdata();

    Organisation organisation1 = mock(Organisation.class);
    when(organisation1.getKey()).thenReturn(101L);
    Organisation organisation2 = mock(Organisation.class);
    when(organisation2.getKey()).thenReturn(102L);

    when(olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashesAndOrganisationIdIn(
                11111111, "11-111-111", List.of(101L, 102L)))
        .thenReturn(List.of(identityAndInstitutionalUserIdentifier1));
    when(olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashesAndOrganisationIdIn(
                22222222, "22-222-222", List.of(101L, 102L)))
        .thenReturn(List.of(identityAndInstitutionalUserIdentifier2));

    List<IdentityAndInstitutionalUserIdentifier> identityAndInstitutionalUserIdentifiers =
        olatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl
            .findIdentitiesAndInstitutionalUserIdentifiersByMatriculationUidsWithOrWithoutDashesAndOrganisations(
                List.of("11111111", "22-222-222"), List.of(organisation1, organisation2));
    assertEquals(2, identityAndInstitutionalUserIdentifiers.size());
    List<Long> identityIds =
        identityAndInstitutionalUserIdentifiers.stream()
            .map(
                identityAndInstitutionalUserIdentifier ->
                    identityAndInstitutionalUserIdentifier.identity().getKey())
            .toList();
    assertTrue(identityIds.contains(101L));
    assertTrue(identityIds.contains(102L));
    List<String> institutionalUserIdentifiers =
        identityAndInstitutionalUserIdentifiers.stream()
            .map(IdentityAndInstitutionalUserIdentifier::institutionalUserIdentifier)
            .toList();
    assertTrue(institutionalUserIdentifiers.contains("11-111-111"));
    assertTrue(institutionalUserIdentifiers.contains("22-222-222"));
  }

  @SuppressWarnings("ExtractMethodRecommender")
  @Test
  void testAppendAdditionalIdentitiesAndInstitutionalUserIdentifiersFoundToFindNamedIdentities() {

    IdentityImpl identity11 = new IdentityImpl();
    identity11.setKey(101L);

    IdentityImpl identity12 = new IdentityImpl();
    identity12.setKey(102L);

    IdentityImpl identity13 = new IdentityImpl();
    identity13.setKey(103L);

    IdentityImpl identity14 = new IdentityImpl();
    identity14.setKey(104L);

    FindNamedIdentity findNamedIdentity11 = new FindNamedIdentity(identity11);
    findNamedIdentity11.addName("nuber");

    FindNamedIdentity findNamedIdentity12 = new FindNamedIdentity(identity12);
    findNamedIdentity12.addName("sweiss");
    findNamedIdentity12.addName("22222222");

    FindNamedIdentity findNamedIdentity14 = new FindNamedIdentity(identity14);
    findNamedIdentity14.addName("rhofer");

    List<FindNamedIdentity> findNamedIdentities =
        new ArrayList<>(List.of(findNamedIdentity11, findNamedIdentity12, findNamedIdentity14));

    IdentityAndInstitutionalUserIdentifier additionalIdentityAndInstitutionalUserIdentifierFound1 =
        new IdentityAndInstitutionalUserIdentifier(identity11, "11-111-111");
    IdentityAndInstitutionalUserIdentifier additionalIdentityAndInstitutionalUserIdentifierFound2 =
        new IdentityAndInstitutionalUserIdentifier(identity12, "22-222-222");
    IdentityAndInstitutionalUserIdentifier additionalIdentityAndInstitutionalUserIdentifierFound3 =
        new IdentityAndInstitutionalUserIdentifier(identity13, "33-333-333");

    OlatExtensionExtendedInstitutionalUserIdentifierSearchServiceImpl
        .appendAdditionalIdentitiesAndInstitutionalUserIdentifiersFoundToFindNamedIdentities(
            List.of(
                additionalIdentityAndInstitutionalUserIdentifierFound1,
                additionalIdentityAndInstitutionalUserIdentifierFound2,
                additionalIdentityAndInstitutionalUserIdentifierFound3),
            List.of("11111111", "22222222", "33-333-333"),
            findNamedIdentities);

    assertEquals(4, findNamedIdentities.size());

    assertEquals(identity11.getKey(), findNamedIdentities.get(0).getIdentity().getKey());
    List<String> names = findNamedIdentities.get(0).getNamesLowerCase();
    assertEquals(2, names.size());
    assertTrue(names.contains("nuber"));
    assertTrue(names.contains("11111111"));

    assertEquals(identity12.getKey(), findNamedIdentities.get(1).getIdentity().getKey());
    names = findNamedIdentities.get(1).getNamesLowerCase();
    assertEquals(2, names.size());
    assertTrue(names.contains("sweiss"));
    assertTrue(names.contains("22222222"));

    assertEquals(identity14.getKey(), findNamedIdentities.get(2).getIdentity().getKey());
    names = findNamedIdentities.get(2).getNamesLowerCase();
    assertEquals(1, names.size());
    assertTrue(names.contains("rhofer"));

    assertEquals(identity13.getKey(), findNamedIdentities.get(3).getIdentity().getKey());
    names = findNamedIdentities.get(3).getNamesLowerCase();
    assertEquals(1, names.size());
    assertTrue(names.contains("33-333-333"));
  }
}
