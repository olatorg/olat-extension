/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.usersearch;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * @author Martin Schraner
 * @since 1.2
 */
class MatriculationUidUtilsTest {

  @Test
  void testIsSearchStringForMatriculationUid() {
    assertTrue(MatriculationUidUtils.isSearchStringForMatriculationUid("01-234-567"));
    assertTrue(MatriculationUidUtils.isSearchStringForMatriculationUid("00-000-000"));
    assertTrue(MatriculationUidUtils.isSearchStringForMatriculationUid("01234567"));
    assertTrue(MatriculationUidUtils.isSearchStringForMatriculationUid("1"));
    assertFalse(MatriculationUidUtils.isSearchStringForMatriculationUid("01234-567"));
    assertFalse(MatriculationUidUtils.isSearchStringForMatriculationUid("01-x34-567"));
    assertFalse(MatriculationUidUtils.isSearchStringForMatriculationUid("x1"));
  }
}
