/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.data.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.olat.basesecurity.IdentityImpl;
import org.olat.core.commons.persistence.DB;
import org.olat.core.id.UserConstants;
import org.olat.extension.model.IdentityAndInstitutionalUserIdentifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author Martin Schraner
 * @since 1.1
 */
@SpringBootTest
@ActiveProfiles("test")
@WebAppConfiguration("target/test-classes")
@ContextConfiguration(classes = OlatExtensionDaoTestConfiguration.class)
@Sql(scripts = "classpath:OlatExtensionIdentityDaoTest_Create.sql")
@Sql(
    scripts = "classpath:OlatExtensionIdentityDaoTest_Delete.sql",
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class OlatExtensionIdentityDaoTest {

  @Autowired private OlatExtensionIdentityDao olatExtensionIdentityDao;
  @Autowired private DB dbInstance;

  @Test
  void testFindUserIdsByInstitutionalUserIdentifierAsIntOrWithDashes() {

    List<Long> userIds =
        olatExtensionIdentityDao.findUserIdsByInstitutionalUserIdentifierAsIntOrWithDashes(
            55555555, "55-555-555");
    assertEquals(1, userIds.size());
    assertEquals(10007L, userIds.get(0));

    userIds =
        olatExtensionIdentityDao.findUserIdsByInstitutionalUserIdentifierAsIntOrWithDashes(
            5555555, "05-555-555");
    assertEquals(1, userIds.size());
    assertEquals(10006L, userIds.get(0));

    userIds =
        olatExtensionIdentityDao.findUserIdsByInstitutionalUserIdentifierAsIntOrWithDashes(
            555555, "00-555-555");
    assertEquals(1, userIds.size());
    assertEquals(10005L, userIds.get(0));

    userIds =
        olatExtensionIdentityDao.findUserIdsByInstitutionalUserIdentifierAsIntOrWithDashes(
            55555, "00-055-555");
    assertEquals(1, userIds.size());
    assertEquals(10004L, userIds.get(0));
  }

  @Test
  void testFindIdentitiesByInstitutionalUserIdentifierAsIntOrWithDashesWithFetchJoinOfUser() {

    List<IdentityImpl> identities =
        olatExtensionIdentityDao
            .findIdentitiesByInstitutionalUserIdentifierAsIntOrWithDashesWithFetchJoinOfUser(
                55555555, "55-555-555");
    assertEquals(1, identities.size());
    assertEquals(107L, identities.get(0).getKey());
    assertEquals(
        "55555555",
        identities.get(0).getUser().getProperty(UserConstants.INSTITUTIONALUSERIDENTIFIER));

    identities =
        olatExtensionIdentityDao
            .findIdentitiesByInstitutionalUserIdentifierAsIntOrWithDashesWithFetchJoinOfUser(
                5555555, "05-555-555");
    assertEquals(1, identities.size());
    assertEquals(106L, identities.get(0).getKey());
    assertEquals(
        "05-555-555",
        identities.get(0).getUser().getProperty(UserConstants.INSTITUTIONALUSERIDENTIFIER));

    identities =
        olatExtensionIdentityDao
            .findIdentitiesByInstitutionalUserIdentifierAsIntOrWithDashesWithFetchJoinOfUser(
                555555, "00-555-555");
    assertEquals(1, identities.size());
    assertEquals(105L, identities.get(0).getKey());
    assertEquals(
        "555555",
        identities.get(0).getUser().getProperty(UserConstants.INSTITUTIONALUSERIDENTIFIER));

    identities =
        olatExtensionIdentityDao
            .findIdentitiesByInstitutionalUserIdentifierAsIntOrWithDashesWithFetchJoinOfUser(
                55555, "00-055-555");
    assertEquals(1, identities.size());
    assertEquals(104L, identities.get(0).getKey());
    assertEquals(
        "00055555",
        identities.get(0).getUser().getProperty(UserConstants.INSTITUTIONALUSERIDENTIFIER));
  }

  @Test
  void
      testFindIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashes() {

    List<IdentityAndInstitutionalUserIdentifier> identitiesAndInstitutionalUserIdentifiers =
        olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashes(
                55555555, "55-555-555");
    assertEquals(1, identitiesAndInstitutionalUserIdentifiers.size());
    assertEquals(107L, identitiesAndInstitutionalUserIdentifiers.get(0).identity().getKey());
    assertEquals(
        "55555555", identitiesAndInstitutionalUserIdentifiers.get(0).institutionalUserIdentifier());

    identitiesAndInstitutionalUserIdentifiers =
        olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashes(
                5555555, "05-555-555");
    assertEquals(1, identitiesAndInstitutionalUserIdentifiers.size());
    assertEquals(106L, identitiesAndInstitutionalUserIdentifiers.get(0).identity().getKey());
    assertEquals(
        "05-555-555",
        identitiesAndInstitutionalUserIdentifiers.get(0).institutionalUserIdentifier());

    identitiesAndInstitutionalUserIdentifiers =
        olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashes(
                555555, "00-555-555");
    assertEquals(1, identitiesAndInstitutionalUserIdentifiers.size());
    assertEquals(105L, identitiesAndInstitutionalUserIdentifiers.get(0).identity().getKey());
    assertEquals(
        "555555", identitiesAndInstitutionalUserIdentifiers.get(0).institutionalUserIdentifier());

    identitiesAndInstitutionalUserIdentifiers =
        olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashes(
                55555, "00-055-555");
    assertEquals(1, identitiesAndInstitutionalUserIdentifiers.size());
    assertEquals(104L, identitiesAndInstitutionalUserIdentifiers.get(0).identity().getKey());
    assertEquals(
        "00055555", identitiesAndInstitutionalUserIdentifiers.get(0).institutionalUserIdentifier());
  }

  @Test
  void
      testFindIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashesAndOrganisationIdIn() {

    List<IdentityAndInstitutionalUserIdentifier> identitiesAndInstitutionalUserIdentifiers =
        olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashesAndOrganisationIdIn(
                55555555, "55-555-555", List.of(102L));
    assertTrue(identitiesAndInstitutionalUserIdentifiers.isEmpty());

    identitiesAndInstitutionalUserIdentifiers =
        olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashesAndOrganisationIdIn(
                55555555, "55-555-555", List.of(103L));
    assertEquals(1, identitiesAndInstitutionalUserIdentifiers.size());
    assertEquals(107L, identitiesAndInstitutionalUserIdentifiers.get(0).identity().getKey());
    assertEquals(
        "55555555", identitiesAndInstitutionalUserIdentifiers.get(0).institutionalUserIdentifier());

    identitiesAndInstitutionalUserIdentifiers =
        olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashesAndOrganisationIdIn(
                5555555, "05-555-555", List.of(102L));
    assertEquals(1, identitiesAndInstitutionalUserIdentifiers.size());
    assertEquals(106L, identitiesAndInstitutionalUserIdentifiers.get(0).identity().getKey());
    assertEquals(
        "05-555-555",
        identitiesAndInstitutionalUserIdentifiers.get(0).institutionalUserIdentifier());

    identitiesAndInstitutionalUserIdentifiers =
        olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashesAndOrganisationIdIn(
                5555555, "05-555-555", List.of(103L));
    assertTrue(identitiesAndInstitutionalUserIdentifiers.isEmpty());

    identitiesAndInstitutionalUserIdentifiers =
        olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashesAndOrganisationIdIn(
                555555, "00-555-555", List.of(102L));
    assertTrue(identitiesAndInstitutionalUserIdentifiers.isEmpty());

    identitiesAndInstitutionalUserIdentifiers =
        olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashesAndOrganisationIdIn(
                555555, "00-555-555", List.of(103L));
    assertEquals(1, identitiesAndInstitutionalUserIdentifiers.size());
    assertEquals(105L, identitiesAndInstitutionalUserIdentifiers.get(0).identity().getKey());
    assertEquals(
        "555555", identitiesAndInstitutionalUserIdentifiers.get(0).institutionalUserIdentifier());

    identitiesAndInstitutionalUserIdentifiers =
        olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashesAndOrganisationIdIn(
                55555, "00-055-555", List.of(102L));
    assertEquals(1, identitiesAndInstitutionalUserIdentifiers.size());
    assertEquals(104L, identitiesAndInstitutionalUserIdentifiers.get(0).identity().getKey());
    assertEquals(
        "00055555", identitiesAndInstitutionalUserIdentifiers.get(0).institutionalUserIdentifier());

    identitiesAndInstitutionalUserIdentifiers =
        olatExtensionIdentityDao
            .findIdentitiesAndInstitutionalUserIdentifiersByInstitutionalUserIdentifierAsIntOrWithDashesAndOrganisationIdIn(
                55555, "00-055-555", List.of(103L));
    assertTrue(identitiesAndInstitutionalUserIdentifiers.isEmpty());
  }

  @AfterEach
  public void afterEach() {
    dbInstance.rollbackTransactionAndCloseEntityManager();
  }
}
