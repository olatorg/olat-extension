DELETE FROM o_bs_group_member WHERE fk_identity_id IN (101, 102, 103, 104, 105, 106, 107);
DELETE FROM o_org_organisation WHERE id = 103;
DELETE FROM o_org_organisation WHERE id = 102;
DELETE FROM o_org_organisation WHERE id = 101;
DELETE FROM o_bs_group WHERE id IN (10001, 10002, 10003);
DELETE FROM o_user WHERE fk_identity IN (101, 102, 103, 104, 105, 106, 107);
DELETE FROM o_bs_identity WHERE id IN (101, 102, 103, 104, 105, 106, 107);