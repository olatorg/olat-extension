INSERT INTO o_bs_group(id, creationdate, g_name)
VALUES (10001, '2022-11-09 12:00:00', NULL);

INSERT INTO o_bs_group(id, creationdate, g_name)
VALUES (10002, '2022-11-09 12:00:00', NULL);

INSERT INTO o_bs_group(id, creationdate, g_name)
VALUES (10003, '2022-11-09 12:00:00', NULL);

INSERT INTO o_org_organisation(id, creationdate, lastmodified, o_identifier, o_displayname,
                               o_description, o_m_path_keys, o_external_id, o_managed_flags,
                               o_status, o_css_class, fk_group, fk_root, fk_parent, fk_type)
VALUES (101, '2022-11-09 12:00:00', '2022-11-09 12:00:00', 'default-org', 'OLAT', NULL,
        '/101/', NULL, 'identifier,externalId,move,delete', 'active', NULL, 10001, NULL, NULL,
        NULL);

INSERT INTO o_org_organisation(id, creationdate, lastmodified, o_identifier, o_displayname,
                               o_description, o_m_path_keys, o_external_id, o_managed_flags,
                               o_status, o_css_class, fk_group, fk_root, fk_parent, fk_type)
VALUES (102, '2022-11-09 12:00:00', '2022-11-12 12:00:00', 'uzh.ch', 'uzh.ch', NULL,
        '/101/102/', NULL, NULL, 'active', NULL, 10002, 101, 101, NULL);

INSERT INTO o_org_organisation(id, creationdate, lastmodified, o_identifier, o_displayname,
                               o_description, o_m_path_keys, o_external_id, o_managed_flags,
                               o_status, o_css_class, fk_group, fk_root, fk_parent, fk_type)
VALUES (103, '2022-11-09 12:00:00', '2022-11-12 12:00:00', 'unilu.ch', 'unilu.ch', NULL,
        '/101/103/', NULL, NULL, 'active', NULL, 10003, 101, 101, NULL);

INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, external_id, status,
                          deleteddate, deletedroles, deletedby, inactivationdate,
                          inactivationemaildate, deletionemaildate,
                          reactivationdate, expirationdate, expirationemaildate)
VALUES (101, 0, '2022-11-09 12:00:00', NULL, 'nuber', NULL, 2, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, external_id, status,
                          deleteddate, deletedroles, deletedby, inactivationdate,
                          inactivationemaildate, deletionemaildate,
                          reactivationdate, expirationdate, expirationemaildate)
VALUES (102, 0, '2022-11-09 12:00:00', NULL, 'lmeier', NULL, 2, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, external_id, status,
                          deleteddate, deletedroles, deletedby, inactivationdate,
                          inactivationemaildate, deletionemaildate,
                          reactivationdate, expirationdate, expirationemaildate)
VALUES (103, 0, '2022-11-09 12:00:00', NULL, 'aschwarz', NULL, 2, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, external_id, status,
                          deleteddate, deletedroles, deletedby, inactivationdate,
                          inactivationemaildate, deletionemaildate,
                          reactivationdate, expirationdate, expirationemaildate)
VALUES (104, 0, '2022-11-09 12:00:00', NULL, 'sweiss', NULL, 2, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, external_id, status,
                          deleteddate, deletedroles, deletedby, inactivationdate,
                          inactivationemaildate, deletionemaildate,
                          reactivationdate, expirationdate, expirationemaildate)
VALUES (105, 0, '2022-11-09 12:00:00', NULL, 'rmoser', NULL, 2, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, external_id, status,
                          deleteddate, deletedroles, deletedby, inactivationdate,
                          inactivationemaildate, deletionemaildate,
                          reactivationdate, expirationdate, expirationemaildate)
VALUES (106, 1, '2022-11-09 12:00:00', NULL, 'ablatter', NULL, 2, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO o_bs_identity(id, version, creationdate, lastlogin, name, external_id, status,
                          deleteddate, deletedroles, deletedby, inactivationdate,
                          inactivationemaildate, deletionemaildate,
                          reactivationdate, expirationdate, expirationemaildate)
VALUES (107, 1, '2022-11-09 12:00:00', NULL, 'rkuster', NULL, 2, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO o_user(user_id, version, creationdate, language, presencemessagespublic,
                   informsessiontimeout, u_firstname, u_lastname, u_nickname,
                   u_institutionaluseridentifier, fk_identity)
VALUES (10001, 0, '2022-11-07 12:00:00', 'DE', FALSE, FALSE, 'Nina', 'Huber', 'nuber',
        '11-111-111', 101);

INSERT INTO o_user(user_id, version, creationdate, language, presencemessagespublic,
                   informsessiontimeout, u_firstname, u_lastname, u_nickname,
                   u_institutionaluseridentifier, fk_identity)
VALUES (10002, 0, '2022-11-07 12:00:00', 'DE', FALSE, FALSE, 'Lea', 'Meier', 'lmeier',
        '22-222-222', 102);

INSERT INTO o_user(user_id, version, creationdate, language, presencemessagespublic,
                   informsessiontimeout, u_firstname, u_lastname, u_nickname,
                   u_institutionaluseridentifier, fk_identity)
VALUES (10003, 0, '2022-11-07 12:00:00', 'DE', FALSE, FALSE, 'Andrea', 'Schwarz', 'aschwarz',
        NULL, 103);

INSERT INTO o_user(user_id, version, creationdate, language, presencemessagespublic,
                   informsessiontimeout, u_firstname, u_lastname, u_nickname,
                   u_institutionaluseridentifier, fk_identity)
VALUES (10004, 0, '2022-11-07 12:00:00', 'DE', FALSE, FALSE, 'Sandra', 'Weiss', 'sweiss',
        '00055555', 104);

INSERT INTO o_user(user_id, version, creationdate, language, presencemessagespublic,
                   informsessiontimeout, u_firstname, u_lastname, u_nickname,
                   u_institutionaluseridentifier, fk_identity)
VALUES (10005, 0, '2022-11-07 12:00:00', 'DE', FALSE, FALSE, 'Reto', 'Moser', 'rmoser',
        '555555', 105);

INSERT INTO o_user(user_id, version, creationdate, language, presencemessagespublic,
                   informsessiontimeout, u_firstname, u_lastname, u_nickname,
                   u_institutionaluseridentifier, fk_identity)
VALUES (10006, 0, '2022-11-07 12:00:00', 'DE', FALSE, FALSE, 'Alexander', 'Blatter', 'ablatter',
        '05-555-555', 106);

INSERT INTO o_user(user_id, version, creationdate, language, presencemessagespublic,
                   informsessiontimeout, u_firstname, u_lastname, u_nickname,
                   u_institutionaluseridentifier, fk_identity)
VALUES (10007, 0, '2022-11-07 12:00:00', 'DE', FALSE, FALSE, 'Reto', 'Kuster', 'rkuster',
        '55555555', 107);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id,
                              fk_identity_id)
VALUES (99991, '2022-11-09 12:00:00', '2022-11-09 12:00:00', 'user', 'none', 10001, 101);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id,
                              fk_identity_id)
VALUES (99971, '2022-11-09 12:00:00', '2022-11-09 12:00:00', 'user', 'none', 10003, 101);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id,
                              fk_identity_id)
VALUES (99992, '2022-11-09 12:00:00', '2022-11-09 12:00:00', 'user', 'none', 10001, 102);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id,
                              fk_identity_id)
VALUES (99982, '2022-11-09 12:00:00', '2022-11-09 12:00:00', 'user', 'none', 10002, 102);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id,
                              fk_identity_id)
VALUES (99993, '2022-11-09 12:00:00', '2022-11-09 12:00:00', 'user', 'none', 10001, 103);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id,
                              fk_identity_id)
VALUES (99973, '2022-11-09 12:00:00', '2022-11-09 12:00:00', 'user', 'none', 10003, 103);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id,
                              fk_identity_id)
VALUES (99994, '2022-11-09 12:00:00', '2022-11-09 12:00:00', 'user', 'none', 10001, 104);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id,
                              fk_identity_id)
VALUES (99984, '2022-11-09 12:00:00', '2022-11-09 12:00:00', 'user', 'none', 10002, 104);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id,
                              fk_identity_id)
VALUES (99995, '2022-11-09 12:00:00', '2022-11-09 12:00:00', 'user', 'none', 10001, 105);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id,
                              fk_identity_id)
VALUES (99975, '2022-11-09 12:00:00', '2022-11-09 12:00:00', 'user', 'none', 10003, 105);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id,
                              fk_identity_id)
VALUES (99996, '2022-11-09 12:00:00', '2022-11-09 12:00:00', 'user', 'none', 10001, 106);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id,
                              fk_identity_id)
VALUES (99986, '2022-11-09 12:00:00', '2022-11-09 12:00:00', 'user', 'none', 10002, 106);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id,
                              fk_identity_id)
VALUES (99997, '2022-11-09 12:00:00', '2022-11-09 12:00:00', 'user', 'none', 10001, 107);

INSERT INTO o_bs_group_member(id, creationdate, lastmodified, g_role, g_inheritance_mode, fk_group_id,
                              fk_identity_id)
VALUES (99977, '2022-11-09 12:00:00', '2022-11-09 12:00:00', 'user', 'none', 10003, 107);